<?php

use Bender\dre_BodyConnect\Application\Controller\Admin\dre_article_main;
use Bender\dre_bodyconnect\Application\Controller\dre_search;
use Bender\dre_BodyConnect\Application\Model\dre_Basket;
use OxidEsales\Eshop\Application\Controller\Admin\ArticleMain;
use OxidEsales\Eshop\Application\Controller\NewsletterController;

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
	'id' => 'dre_bodyconnect',
	'title' => '<img src="../modules/bender/dre_bodyconnect/out/img/favicon.ico" title="Bodynova Klassenmodul Theme">odynova Verbindung nach Hause',
	'description' => [
		'de' => 'Bodynova Verbindung nach Hause',
		'en' => 'Bodynova connect @ home'
	],
	'version' => '1.5.0',
	'author' => 'André Bender',
	'url' => 'https://bodynova.de',
	'thumbnail' => 'out/img/logo_bodynova.png',
	'email' => 'support@bodynova.de',
	'controllers' => array(

		/*
		 TODO:  DRE wird nicht überschrieben klären warum nicht!!!
		'dre_articlelistcontroller'
		=> \Bender\dre_BodyConnect\Application\Controller\dre_ArticleListController::class,
		*/

		'dre_recommarticlelistcontroller'
		=> \Bender\dre_BodyConnect\Application\Controller\dre_RecommArticleListController::class,

		'bnversand'
		=> \Bender\dre_BodyConnect\Application\Controller\bnversand::class,

		'dredetails'
		=> \Bender\dre_BodyConnect\Application\Controller\dre_articledetailscontroller::class


	),
	'extend' => array(

		\OxidEsales\Eshop\Application\Controller\Admin\ArticleMain::class =>
			\Bender\dre_BodyConnect\Application\Controller\Admin\dre_article_main::class,

		\OxidEsales\Eshop\Application\Controller\NewsletterController::class =>
			\Bender\dre_BodyConnect\Application\Controller\dre_NewsletterController::class,

		\OxidEsales\Eshop\Application\Controller\ArticleListController::class =>
			\Bender\dre_BodyConnect\Application\Controller\dre_ArticleListController::class,

		\OxidEsales\Eshop\Application\Controller\Admin\OrderOverview::class =>
			\Bender\dre_BodyConnect\Application\Controller\Admin\dre_orderoverview::class,

		\OxidEsales\Eshop\Application\Controller\Admin\CategoryMain::class =>
			\Bender\dre_BodyConnect\Application\Controller\Admin\dre_category_main::class,

		\OxidEsales\Eshop\Application\Controller\Admin\CategoryMainAjax::class =>
			\Bender\dre_BodyConnect\Application\Controller\Admin\dre_category_main_ajax::class,

		\OxidEsales\Eshop\Application\Controller\Admin\OrderMain::class =>
			\Bender\dre_BodyConnect\Application\Controller\Admin\dre_ordermain::class,

		\OxidEsales\Eshop\Application\Model\ArticleList::class =>
			\Bender\dre_BodyConnect\Application\Model\dre_ArticleList::class,

		\OxidEsales\Eshop\Application\Model\Article::class =>
			\Bender\dre_BodyConnect\Application\Model\dre_Article::class,

		\OxidEsales\Eshop\Application\Model\Basket::class =>
			\Bender\dre_BodyConnect\Application\Model\dre_Basket::class,

		\OxidEsales\Eshop\Application\Model\BasketItem::class =>
			\Bender\dre_BodyConnect\Application\Model\dre_BasketItem::class,

		\OxidEsales\Eshop\Application\Model\CategoryList::class =>
			\Bender\dre_BodyConnect\Application\Model\dre_CategoryList::class,


		\OxidEsales\Eshop\Application\Component\Widget\CategoryTree::class =>
			\Bender\dre_BodyConnect\Application\Component\Widget\dre_CategoryTree::class,

		\OxidEsales\Eshop\Core\Price::class =>
			\Bender\dre_BodyConnect\Core\dre_Price::class,

		\OxidEsales\Eshop\Application\Component\Widget\ServiceMenu::class =>
			\Bender\dre_BodyConnect\Application\Component\Widget\dre_ServiceMenu::class

	),
	'blocks' => array(
		array(
			'template' => 'page/details/inc/productmain.tpl',
			'block' => 'details_productmain_tprice',
			'file' => 'Application/views/blocks/dre_details_productmain_tprice.tpl'
		),
		array(
			'template' => 'layout/footer.tpl',
			'block' => 'dd_footer_servicelist_inner',
			'file' => 'Application/views/blocks/layout/dre_footer_servicelist_inner.tpl'
		),
		array(
			'template' => 'layout/footer.tpl',
			'block' => 'dd_footer_information_inner',
			'file' => 'Application/views/blocks/layout/dre_footer_information_inner.tpl'
		),
		array(
			'template' => 'layout/footer.tpl',
			'block' => 'dd_footer_manufacturerlist',
			'file' => 'Application/views/blocks/layout/dre_footer_product.tpl'
		),
		array(
			'template' => 'layout/footer.tpl',
			'block' => 'dd_footer_social_links',
			'file' => 'Application/views/blocks/layout/dre_footer_social.tpl'
		),
		array(
			'template' => 'category_list.tpl',
			'block' => 'admin_category_list_colgroup',
			'file' => 'Application/views/blocks/dre_admin_category_list_colgroup.tpl'
		),
		array(
			'template' => 'category_list.tpl',
			'block' => 'admin_category_list_filter',
			'file' => 'Application/views/blocks/dre_admin_category_list_filter.tpl'
		),
		array(
			'template' => 'category_list.tpl',
			'block' => 'admin_category_list_sorting',
			'file' => 'Application/views/blocks/dre_admin_category_list_sorting.tpl'
		),
		array(
			'template' => 'category_list.tpl',
			'block' => 'admin_category_list_item',
			'file' => 'Application/views/blocks/dre_admin_category_list_item.tpl'
		),
		array(
			'template' => 'article_variant.tpl',
			'block' => 'admin_article_variant_listheader',
			'file' => 'Application/views/blocks/dre_admin_article_variant_listheader.tpl'
		),
		array(
			'template' => 'article_variant.tpl',
			'block' => 'admin_article_variant_listitem',
			'file' => 'Application/views/blocks/dre_admin_article_variant_listitem.tpl'
		),
		/*
		array(
			'template' => 'article_variant.tpl',
			'block' => 'admin_article_variant_selectlist',
			'file' => 'Application/views/blocks/dre_admin_article_variant_selectlist.tpl'
		),
		*/

	),
	'templates' => array(
		'dre_recommendations.tpl' => 'bender/dre_bodyconnect/Application/views/tpl/page/dre_recommendations/dre_recommlist.tpl',
		'versand.tpl' => 'bender/dre_bodyconnect/Application/views/tpl/versand.tpl',
		// admin
		'dre_category_list.tpl' => 'bender/dre_bodyconnect/Application/views/admin/tpl/dre_category_list.tpl',
		'dre_category_main.tpl' => 'bender/dre_bodyconnect/Application/views/admin/tpl/dre_category_main.tpl',
		'dre_category_main_form.tpl' => 'bender/dre_bodyconnect/Application/views/admin/tpl/include/dre_category_main_form.tpl',
		'dre_orderoverview.tpl' => 'bender/dre_bodyconnect/Application/views/admin/tpl/dre_orderoverview.tpl',
		'dre_ordermain.tpl' => 'bender/dre_bodyconnect/Application/views/admin/tpl/dre_ordermain.tpl',

		'dre_sidebar_categorytree.tpl' => 'bender/dre_bodyconnect/Application/views/tpl/widget/sidebar/dre_sidebar_categorytree.tpl',
		'dre_header_categorytree.tpl' => 'bender/dre_bodyconnect/Application/views/tpl/widget/header/dre_header_categorytree.tpl',
		'dre_article_main.tpl' => 'bender/dre_bodyconnect/Application/views/admin/tpl/dre_article_main.tpl',
		'detailsmin.tpl' => 'bender/dre_bodyconnect/Application/views/tpl/page/details/detailsmin.tpl'

	),
	'events' => array(/*
		'onActivate'                        => 'Bender\dre_CategorySlider\Core\Events::onActivate',
		'onDeactivate'                      => 'Bender\dre_CategorySlider\Core\Events::onDeactivate',
		*/
	),
    'smartyPluginDirectories' => [
        'Core/Smarty/Plugin'
    ],
);
