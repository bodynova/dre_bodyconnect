<?php

namespace Bender\dre_BodyConnect\Application\Component\Widget;

use Bender\dre_BodyConnect\Application\Model\dre_CategoryList;
use OxidEsales\EshopCommunity\Internal\Framework\Templating\Loader\TemplateLoaderInterface;

class dre_CategoryTree extends dre_CategoryTree_parent
{
	/**
	 * Current class template name.
	 *
	 * @var string
	 */
	protected $_sThisTemplate = 'dre_sidebar_categorytree.tpl';
	//protected $_sThisTemplate = 'widget/sidebar/categorytree.tpl';

	/**
	 * Executes parent::render(), assigns template name and returns it
	 *
	 * @return string
	 */
	public function render()
	{
		parent::render();

		/*$categorylist = oxNew(\Bender\dre_BodyConnect\Application\Model\dre_CategoryList::class);
		$categorylist->setWelt($this->getWeltList());
		$categorylist->dre_loadWeltList($this->getWeltList());
		$this->categorylist = $categorylist;*/
		if ($sTpl = $this->getViewParameter("sWidgetType")) {
			return 'dre_' . basename($sTpl) . '_categorytree.tpl';
		}
		return $this->_sThisTemplate;
	}

	public function getWeltList()
	{
		return $this->getViewParameter('welt');
	}
}
