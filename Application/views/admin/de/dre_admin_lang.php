<?php
/**
 * Copyright © OXID eSales AG. All rights reserved.
 * See LICENSE file for license details.
 */

$sLangName  = "Deutsch";

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset'                                                     => 'UTF-8',
    'fullDateFormat'                                              => 'd.m.Y H:i:s',
    'simpleDateFormat'                                            => 'd.m.Y',
    'GENERAL_TITLE_WELT'                                          => 'WELTEN VERANKERUNG',
    'CATEGORY_MAIN_WELT'                                          => 'WELT 1-5',
    'VersandHide'                                                 => 'Versand Versteckt'
];