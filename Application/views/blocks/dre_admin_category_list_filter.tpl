<td valign="top" class="listfilter first" height="20">
    <div class="r1"><div class="b1">&nbsp;test</div></div>
</td>
<td valign="top" class="listfilter" height="20" align="center">
    <div class="r1">
        <div class="b1">
            <input class="listedit" type="text" size="5" maxlength="128" name="where[oxcategories][oxsort]" value="[{$where.oxcategories.oxsort}]">
        </div>
    </div>
</td>
<td valign="top" class="listfilter" height="20" >
    <div class="r1"><div class="b1">
            <div class="find">
                <select name="changelang" class="editinput" onChange="Javascript:top.oxid.admin.changeLanguage();">
                    [{foreach from=$languages item=lang}]
                        <option value="[{$lang->id}]" [{if $lang->selected}]SELECTED[{/if}]>[{$lang->name}]</option>
                    [{/foreach}]
                </select>
                <input class="listedit" type="submit" name="submitit" value="[{oxmultilang ident="GENERAL_SEARCH"}]">
            </div>

            <select name="where[oxcategories][oxparentid]" class="editinput" onChange="Javascript:document.search.submit();">
                [{foreach from=$cattree->aList item=pcat}]
                    <option value="[{$pcat->oxcategories__oxid->value}]" [{if $pcat->selected}]SELECTED[{/if}]>[{$pcat->oxcategories__oxtitle->getRawValue()}][{if isset($pcat->oxcategories__welt)}] || Welt: [{$pcat->oxcategories__welt->getRawValue()}][{else}]<span>zuordnen</span>[{/if}]</option>
                [{/foreach}]
            </select>

            <input class="listedit" type="text" size="50" maxlength="128" name="where[oxcategories][oxtitle]" value="[{$where.oxcategories.oxtitle}]">
        </div>
    </div>
</td>
<td valign="top" class="listfilter" height="20" align="center">
    <div class="r1">
        <div class="b1">
            <input class="listedit" type="text" size="5" maxlength="128" name="where[oxcategories][welt]" value="[{$where.oxcategories.welt}]">
        </div>
    </div>
</td>