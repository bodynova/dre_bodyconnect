[{oxifcontent ident="bodyfooterprodukt" object="oContent"}]
    <section class="col-xs-12 col-sm-6 col-lg-3 footer-box footer-box-manufacturers productinfo">
        <div class="h4 footerhead">[{$oContent->oxcontents__oxtitle->value}]</div>
        <div class="footer-box-content">
            [{block name="dd_footer_manufacturerlist_inner"}]
                [{$oContent->oxcontents__oxcontent->value}]
                [{*oxid_include_widget cl="oxwManufacturerList" _parent=$oView->getClassName() noscript=1 nocookie=1*}]
            [{/block}]
        </div>
    </section>
[{/oxifcontent}]

