[{* Social Links ersetzt *}]

[{* original: *}]

[{*if $oViewConf->getViewThemeParam('sFacebookUrl') || $oViewConf->getViewThemeParam('sGooglePlusUrl') || $oViewConf->getViewThemeParam('sTwitterUrl') || $oViewConf->getViewThemeParam('sYouTubeUrl') || $oViewConf->getViewThemeParam('sBlogUrl')}]
    <div class="social-links">
        <div class="row">
            <section class="col-xs-12">
                <div class="text-center">
                    [{block name="dd_footer_social_links_inner"}]
                        <ul class="list-inline">
                            [{block name="dd_footer_social_links_list"}]
                                [{if $oViewConf->getViewThemeParam('sFacebookUrl')}]
                                    <li>
                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sFacebookUrl')}]">
                                            <i class="fa fa-facebook"></i> <span>Facebook</span>
                                        </a>
                                    </li>
                                [{/if}]
                                [{if $oViewConf->getViewThemeParam('sGooglePlusUrl')}]
                                    <li>
                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sGooglePlusUrl')}]">
                                            <i class="fa fa-google-plus-square"></i> <span>Google+</span>
                                        </a>
                                    </li>
                                [{/if}]
                                [{if $oViewConf->getViewThemeParam('sTwitterUrl')}]
                                    <li>
                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sTwitterUrl')}]">
                                            <i class="fa fa-twitter"></i> <span>Twitter</span>
                                        </a>
                                    </li>
                                [{/if}]
                                [{if $oViewConf->getViewThemeParam('sYouTubeUrl')}]
                                    <li>
                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sYouTubeUrl')}]">
                                            <i class="fa fa-youtube-square"></i> <span>YouTube</span>
                                        </a>
                                    </li>
                                [{/if}]
                                [{if $oViewConf->getViewThemeParam('sBlogUrl')}]
                                    <li>
                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sBlogUrl')}]">
                                            <i class="fa fa-wordpress"></i> <span>Blog</span>
                                        </a>
                                    </li>
                                [{/if}]
                            [{/block}]
                        </ul>
                    [{/block}]
                </div>
            </section>
        </div>
    </div>
[{/if*}]