[{capture append="oxidBlock_content"}]
[{*<div class="container">*}]
    <div class="col-xs-12">
        [{* smooth scrolling *}]
        [{oxscript add="
            $(document).ready(function(){
                $( '#accordion' ).accordion({
                    heightStyle: 'content',
                    icons: false,
                    collapsible: true,
                    active:false,
                    activate: function( event, ui ) {
                        if(!$.isEmptyObject(ui.newHeader.offset())) {
                            $('html:not(:animated), body:not(:animated)').animate({ scrollTop: ui.newHeader.offset().top }, 'slow');
                        }
                    }
                });
            });
            "
        }]
        [{* CMS Seite Versand und Kosten *}]
        [{oxifcontent ident="0af41238fdb98bb20f620331caab0a32" object="oContent"}]
            <h1 class="page-header" style="text-align: center">[{$oContent->oxcontents__oxtitle->value}]</h1>
            [{$oContent->oxcontents__oxcontent->value}]
        [{/oxifcontent}]
        [{* Liste über alle Länder *}]
        [{assign var='landliste' value=$oView->getCountryList()}]
        [{assign var='zahl' value=0}]
        [{* Ein Panel pro Land *}]
        <div id="accordion" class="col-lg-8 col-lg-offset-2" style="padding:0">
            [{foreach from=$landliste item=oLand}]
                <h3 class="panel panel-default panel[{$zahl}]" style="margin-bottom: 0;margin-top:0;background-color: #F0F0F0;padding:3px">
                    [{$oLand->oxcountry__oxtitle->value}]
                </h3>
                    [{* Panel Body *}]
                    <div id="collapse[{$zahl}]" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="überschrift[{$zahl}]" aria-expanded="false">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-condensed">
                                    <tr align="left">
                                        <th>
                                            [{oxmultilang ident="SHIPPING_CARRIER"}]
                                        </th>
                                        <th style="text-align:right">
                                            [{oxmultilang ident="SHIPPING_COST"}]
                                        </th>
                                    </tr>
                                    [{* Schleife über alle Versandsets die dem entsprechenden Land zugeordnet sind *}]
                                    [{assign var='deliverysets' value=$oView->getDeliverySets($oLand->oxcountry__oxid->value)}]
                                    [{*$deliverysets|var_dump*}]
                                    [{foreach from=$deliverysets item=deliveryset}]
                                        [{*$deliveryset|var_dump*}]
                                        [{* Das Versandset als Objekt *}]
                                        [{assign var='oDelivery' value=$oView->getDelivery($deliveryset[0])}]
                                        [{*$oDelivery|var_dump*}]
                                        [{* alle Kategorien die dem Versandset zugeordnet sind *}]
                                        [{assign var='arrCategorielist' value=$oView->getCategories($deliveryset[0])}]
                                        [{* Ausgabe des Titels des Versandsets, wenn das Land entsprechend eingestellt ist *}]
                                        [{if $oView->isCountrySetToShowVersandset($oLand->oxcountry__oxid->value) == 1 && $oDelivery->oxdelivery__oxtitle->value }]
                                            <tr>
                                                <td colspan="2"><b>[{$oDelivery->oxdelivery__oxtitle->value}]</b>
                                                </td>
                                            </tr>
                                        [{/if}]
                                        [{* Schleife über die gefundenen KAtegorien *}]
                                        [{foreach from=$arrCategorielist item=categorie}]
                                            [{* Die Kategorie als Objekt *}]
                                            [{assign var='catobj' value=$oView->getCategoryObject($categorie.OXID)}]
                                            [{if $oDelivery !== false}]
                                                <tr>
                                                    <td style="padding-left: 30px;white-space: inherit">
                                                        [{* Kategorientitel ohne das beginnende Wort Versand *}]
                                                        [{$catobj->oxcategories__oxtitle->value|regex_replace:"/(Versand)/":""}][{*$oDelivery->oxdelivery__oxtitle->value*}]
                                                    </td>
                                                    <td style="text-align:right;white-space: inherit">
                                                        [{* Ausgabe der Versandkosten, wenn Land Mwst pflichtig mit, ansonsten ohne*}]
                                                        [{*if $oLand->oxcountry__oxvatstatus->value == 1}]
                                                            [{math assign="preis" equation="x * y" x=$oDelivery->oxdelivery__oxaddsum->value y=1.19 format="%.2f"}]
                                                            [{if $preis == 0}]
                                                            [{else}]
                                                                [{oxmultilang ident="PREIS" suffix="COLON"}]
                                                                <b>[{$preis|replace:".":","}]</b>
                                                                €
                                                            [{/if}]
                                                        [{else*}]
                                                            [{math assign="preis" equation="x * y" x=$oDelivery->oxdelivery__oxaddsum->value y=1 format="%.2f"}]
                                                            [{if $preis == 0}]
                                                            [{else}]
                                                                [{oxmultilang ident="PREIS" suffix="COLON"}]
                                                                <b>[{$preis|replace:".":","}]</b>
                                                                €
                                                            [{/if}]
                                                        [{*/if*}]
                                                        [{* Wenn keine Kosten definiert sind ausgabe: Kostenlos *}]
                                                        [{if $oDelivery->oxdelivery__oxaddsum->value == 0}]
                                                            [{*oxmultilang ident="FREE"*}]
                                                        [{/if}]
                                                    </td>
                                                </tr>
                                            [{/if}]
                                        [{/foreach}]
                                    [{/foreach}]
                                </table>
                            </div>
                        </div>
                    </div>
                [{assign var='zahl' value=$zahl+1}]
            [{/foreach}]
        </div>

    </div>
[{*</div>*}]
[{/capture}]
[{include file="layout/page.tpl"}]


[{*

foreach from=$landliste item=oLand}]
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 0">
        <div class="panel panel-default panel[{$zahl}]">
            [{* Land Titel als Panel Head }]
            <div class="panel-heading" role="tab" id="überschrift[{$zahl}]">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse[{$zahl}]"
                   aria-expanded="false" aria-controls="collapse[{$zahl}]">
                    <h4 class="panel-title"
                        style='font-family: "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif !important;'>
                        [{$oLand->oxcountry__oxtitle->value}]
                    </h4>
                </a>
            </div>
            [{* Panel Body }]
            <div id="collapse[{$zahl}]" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="überschrift[{$zahl}]" aria-expanded="false">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-condensed">
                            <tr align="left">
                                <th>
                                    [{oxmultilang ident="SHIPPING_CARRIER"}]
                                </th>
                                <th style="text-align:right">
                                    [{oxmultilang ident="SHIPPING_COST"}]
                                </th>
                            </tr>
                            [{* Schleife über alle Versandsets die dem entsprechenden Land zugeordnet sind }]
                            [{assign var='deliverysets' value=$oView->getDeliverySets($oLand->oxcountry__oxid->value)}]
                            [{foreach from=$deliverysets item=deliveryset}]
                                [{* Das Versandset als Objekt }]
                                [{assign var='oDelivery' value=$oView->getDelivery($deliveryset[0])}]
                                [{* alle Kategorien die dem Versandset zugeordnet sind }]
                                [{assign var='arrCategorielist' value=$oView->getCategories($deliveryset[0])}]
                                [{* Ausgabe des Titels des Versandsets, wenn das Land entsprechend eingestellt ist }]
                                [{if $oView->isCountrySetToShowVersandset($oLand->oxcountry__oxid->value) == 1 && $oDelivery->oxdelivery__oxtitle->value }]
                                    <tr>
                                        <td colspan="2"><b>[{$oDelivery->oxdelivery__oxtitle->value}]</b></td>
                                    </tr>
                                [{/if}]
                                [{* Schleife über die gefundenen KAtegorien }]
                                [{foreach from=$arrCategorielist item=categorie}]
                                    [{* Die Kategorie als Objekt }]
                                    [{assign var='catobj' value=$oView->getCategoryObject($categorie.OXID)}]
                                    [{if $oDelivery !== false}]
                                        <tr>
                                            <td style="padding-left: 30px;white-space: inherit">
                                                [{* Kategorientitel ohne das beginnende Wort Versand }]
                                                [{$catobj->oxcategories__oxtitle->value|regex_replace:"/(Versand)/":""}][{*$oDelivery->oxdelivery__oxtitle->value}]
                                            </td>
                                            <td style="text-align:right;white-space: inherit">
                                                [{* Ausgabe der Versandkosten, wenn Land Mwst pflichtig mit, ansonsten ohne}]
                                                [{if $oLand->oxcountry__oxvatstatus->value == 1}]
                                                    [{math assign="preis" equation="x * y" x=$oDelivery->oxdelivery__oxaddsum->value y=1.19 format="%.2f"}]
                                                    [{if $preis == 0}]
                                                    [{else}]
                                                        [{oxmultilang ident="PREIS" suffix="COLON"}]
                                                        <b>[{$preis|replace:".":","}]</b>
                                                        €
                                                    [{/if}]
                                                [{else}]
                                                    [{math assign="preis" equation="x * y" x=$oDelivery->oxdelivery__oxaddsum->value y=1 format="%.2f"}]
                                                    [{if $preis == 0}]
                                                    [{else}]
                                                        [{oxmultilang ident="PREIS" suffix="COLON"}]
                                                        <b>[{$preis|replace:".":","}]</b>
                                                        €
                                                    [{/if}]
                                                [{/if}]
                                                [{* Wenn keine Kosten definiert sind ausgabe: Kostenlos }]
                                                [{if $oDelivery->oxdelivery__oxaddsum->value == 0}]
                                                    [{*oxmultilang ident="FREE"}]
                                                [{/if}]
                                            </td>
                                        </tr>
                                    [{/if}]
                                [{/foreach}]
                            [{/foreach}]
                        </table>
                    </div>
                </div>
            </div>
        </div>
        [{assign var='zahl' value=$zahl+1}]
    </div>
[{/foreach

*}]