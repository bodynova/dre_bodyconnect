
[{block name="dd_widget_sidebar_categorytree"}]


	[{if $oxcmp_categories}]

		[{if $categories != null}]
			[{assign var="categories" value=$oxcmp_categories->getClickRoot()}]
		[{else}]
			[{assign var="categories" value=$oxcmp_categories->getArray()}]
		[{/if}]

		[{assign var="oxcmp_categories" value=$oxcmp_categories}]

		[{assign var="act" value=$oxcmp_categories->getClickCat()}]
		[{assign var="actint" value=$act->oxcategories__welt->value|intval}]

		[{assign var="weltstring" value='welt'|cat:$act->oxcategories__welt->value}]
		[{*$weltstring*}]
		[{oxscript add="
			$(document).ready(function(){
				//$('#BN-Sidebar').affix();
				/*$('#BN-Sidebar').on('hover', function(){

				});*/
			});
		"}]

		<div id="BN-Sidebar" class="nav hidden-xs hidden-sm" style="background-color: white">

			<h3 id="welttitel" style="font-weight: lighter;color:#808080">
				[{oxifcontent ident=$weltstring object="oCont"}]
					[{$oCont->oxcontents__oxtitle->value}]
				[{/oxifcontent}]
				[{*}]
				<strong>
					<i class="fa fa-globe"></i>
				</strong>
				[{*}]
			</h3>

			[{assign var="oCat" value=$oxcmp_categories->dre_loadWeltList($actint)}]


			[{assign var="welt" value=$oxcmp_categories->getWelt()}]
			[{*$welt|var_dump*}]
			[{if $oViewConf->getTopActionClassName() == 'start'}]
				[{assign var="homeSelected" value="true"}]
			[{/if}]
			[{assign var='actCatId' value=$oViewConf->getActCatId()}]
			[{assign var="level1counter" value="0"}]

			[{foreach from=$oxcmp_categories item="ocat" key="catkey" name="root"}]
				[{assign var="level1counter" value=$level1counter+1}]
				<div class="panel-group" id="accordion[{$level1counter}]" role="tablist" aria-multiselectable="true" style="margin-bottom: 0;">

					[{if $ocat->oxcategories__welt->value == $welt}]
						[{assign var='active' value='true'}]
					[{else}]
						[{assign var='active' value='false'}]
					[{/if}]
					[{if $ocat->getIsVisible()}]
			[{* Erste Ebene *}]
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="überschrift[{$level1counter}]">
									<h4 class="panel-title" style="background-color:#F3F3F3;color:#777;">
										<a href="[{$ocat->getLink()}]" style="margin-right: 3px;[{if $oxcmp_categories->dre_getActive($ocat->oxcategories__oxid->value) == true}]font-weight:bold;text-decoration: underline;[{/if}]" >[{$ocat->oxcategories__oxtitle->value|html_entity_decode}]</a>
										[{if $ocat->getSubCats()}]
											<a role="button" data-toggle="collapse" data-parent="#accordion[{$level1counter}]" href="#collapse[{$level1counter}]"
											   aria-expanded="true" aria-controls="collapse[{$level1counter}]" class="pull-right" onclick="if($(this).children().hasClass('fa-chevron-down')){$(this).children().removeClass('fa-chevron-down').addClass('fa-chevron-up')}else{$(this).children().removeClass('fa-chevron-up').addClass('fa-chevron-down')}">
												<i class="fal [{if $oxcmp_categories->dre_getActive($ocat->oxcategories__oxid->value) == true}]fa-chevron-up[{else}]fa-chevron-down[{/if}]" aria-hidden="true"></i>
											</a>
										[{/if}]
									</h4>
								</div>
								[{if $ocat->getSubCats()}]
									<div id="collapse[{$level1counter}]" class="panel-collapse collapse [{if $oxcmp_categories->dre_getActive($ocat->oxcategories__oxid->value) == true}]in[{/if}]" role="tabpanel" aria-labelledby="überschrift[{$level1counter}]">
										<div class="panel-body">
						[{* Zweite Ebene *}]
											<ul class="sidebar-panel-inhalt" style="[{*if $oxcmp_categories->dre_getActive($ocat->oxcategories__oxid->value) == true}]display:block;[{else}]display: none;[{/if*}]list-style: none;padding-left: 15px">
												[{foreach from=$ocat->getSubCats() item="osubcat" key="subcatkey" name="SubCat"}]
													[{if $osubcat->getIsVisible()}]
														[{* CMS - Kategorien Schleife *}]
														[{* foreach from=$osubcat->getContentCats() item=ocont name=MoreCms}]
															<li>
																<a href="[{$ocont->getLink()}]">
																	[{$ocont->oxcontents__oxtitle->value}]
																</a>
															</li>
														[{/foreach *}]
													[{/if}]
													[{* zweite Ebene *}]
													[{if $osubcat->getIsVisible()}]
														<li class="[{if $osubcat->getSubCats()}]has-sub[{/if}]">
															<div class="btn-group" style="width: 100%;">
																[{* Titel *}]
																<a style="padding:1px 7px;white-space:normal;text-align: left;"
																   class="btn [{if $oxcmp_categories->dre_getActive($osubcat->oxcategories__oxid->value) == true}]bold[{/if}][{if $oViewConf->getActCatId() == $osubcat->oxcategories__oxid->value}] active[{/if}]"
																   href="[{$osubcat->getLink()}]">
																	[{$osubcat->oxcategories__oxtitle->value|html_entity_decode}]
																</a>
																[{if $osubcat->getSubCats()}]
																	[{* +/- Button *}]
																	<a style="padding:1px 6px;cursor: pointer;font-size:14px;margin-right: -3px"
																	   class="pull-right no-hover"
																	   onclick="$(this).parent().parent().children('ul').toggle();if($(this).children('i').hasClass('fa-chevron-down')){$(this).children('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');}else{$(this).children('i').removeClass('fa-chevron-up').addClass('fa-chevron-down')}">
																		[{if $oxcmp_categories->dre_getActive($osubcat->oxcategories__oxid->value) == true}]
																			<i class="fal fa-chevron-up"></i>
																		[{else}]
																			<i class="fal fa-chevron-down"></i>
																		[{/if}]
																	</a>
																[{/if}]
															</div>
															[{if $osubcat->getSubCats()}]
																[{* Subcategorien *}]
																<ul style="[{if $oxcmp_categories->dre_getActive($osubcat->oxcategories__oxid->value) == true}]display:block;[{else}]display: none;[{/if}]list-style: none;padding-left: 15px">
																	[{foreach from=$osubcat->getSubCats() item="oosubcat" key="subcatkey" name="SubCat"}]
																		[{if $oosubcat->getIsVisible()}]
																			[{* CMS - Kategorien Schleife *}]
																			[{* foreach from=$oosubcat->getContentCats() item=subocont name=MoreCms}]
																				<li>
																					<a href="[{$subocont->getLink()}]">[{$subocont->oxcontents__oxtitle->value}]</a>
																				</li>
																			[{/foreach *}]
																		[{/if}]
																		[{* dritte Ebene *}]
																		[{if $oosubcat->getIsVisible()}]
																			<li class="[{if $oosubcat->getSubCats()}]has-sub[{/if}]">
																				<div class="btn-group"
																				     style="width: 100%;">
																					[{* Titel *}]
																					<a style="padding:1px 7px;white-space:normal;text-align: left;margin-right: -3px;"
																					   class="btn [{if $oxcmp_categories->dre_getActive($oosubcat->oxcategories__oxid->value) == true}]bold[{/if}][{if $oViewConf->getActCatId() == $oosubcat->oxcategories__oxid->value}] active[{/if}]"
																					   href="[{$oosubcat->getLink()}]">
																						[{$oosubcat->oxcategories__oxtitle->value|html_entity_decode}]
																					</a>

																					[{if $oosubcat->getSubCats()}]
																						[{* +/- Button *}]
																						<a style="padding:0;cursor: pointer;font-size:14px;line-height:26px;"
																						   class="pull-right no-hover"
																						   onclick="$(this).parent().parent().children('ul').toggle();if($(this).children('i').hasClass('fa-chevron-down')){$(this).children('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');}else{$(this).children('i').removeClass('fa-chevron-up').addClass('fa-chevron-down')}">
																							[{if $oxcmp_categories->dre_getActive($oosubcat->oxcategories__oxid->value) == true}]
																								<i class="fal fa-chevron-up"></i>
																							[{else}]
																								<i class="fal fa-chevron-down"></i>
																							[{/if}]
																						</a>
																					[{/if}]
																				</div>
																				[{if $oosubcat->getSubCats()}]
																					[{* Subcategorien *}]
																					<ul style="[{if $oxcmp_categories->dre_getActive($oosubcat->oxcategories__oxid->value) == true}]display:block;[{else}]display: none;[{/if}]list-style: none;padding-left: 15px">
																						[{foreach from=$oosubcat->getSubCats() item="moosubcat" key="subcatkey" name="SubCat"}]
																							[{if $moosubcat->getIsVisible()}]
																								[{* CMS - Kategorien Schleife *}]
																								[{* foreach from=$moosubcat->getContentCats() item=subocont name=MoreCms}]
																									<li>
																										<div style="border-left:1px solid">
																											<a class="btn" href="[{$subocont->getLink()}]">
																												[{$subocont->oxcontents__oxtitle->value}]
																											</a>
																										</div>
																									</li>
																								[{/foreach *}]
																							[{/if}]
																							[{if $moosubcat->getIsVisible()}]
																								<li>
																									<div class="btn-group"
																									     style="width: 100%;">
																										<a style="padding:1px 7px;white-space:normal;text-align: left;"
																										   class="btn [{if $oxcmp_categories->dre_getActive($moosubcat->oxcategories__oxid->value) == true}]bold[{/if}][{if $oViewConf->getActCatId() == $moosubcat->oxcategories__oxid->value}] active[{/if}]"
																										   href="[{$moosubcat->getLink()}]">
																											[{$moosubcat->oxcategories__oxtitle->value|html_entity_decode}]
																										</a>
																									</div>
																								</li>
																							[{/if}]
																						[{/foreach}]
																					</ul>
																				[{/if}]
																			</li>
																			[{*}]<hr class="hr-welt"/>[{*}]
																		[{/if}]
																	[{/foreach}]
																</ul>
															[{/if}]
														</li>
													[{/if}]
												[{/foreach}]
											</ul>
										</div>
									</div>
								[{/if}]
							</div>
					[{/if}]
				</div>
			[{/foreach}]

		</div>
	[{/if}]
[{/block}]
