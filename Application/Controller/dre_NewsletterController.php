<?php

namespace Bender\dre_BodyConnect\Application\Controller;

class dre_NewsletterController extends \OxidEsales\Eshop\Application\Controller\NewsletterController
{
    public function getUserEmail(){
        $oUser = oxNew(\OxidEsales\Eshop\Application\Model\User::class);
        $oUser->load(\OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('uid'));
        return $oUser->oxuser__oxusername->value;
    }
}