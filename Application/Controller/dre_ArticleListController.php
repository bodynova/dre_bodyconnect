<?php
//\OxidEsales\Eshop\Application\Controller\ArticleListController
//\OxidEsales\Eshop\Application\Controller\SearchController

namespace Bender\dre_BodyConnect\Application\Controller;

class dre_ArticleListController extends \OxidEsales\Eshop\Application\Controller\ArticleListController
{

    /**
     * List type
     *
     * @var string
 *
* protected $_sListType = 'recommlist';
     */
    /**
     * Current class template name.
     *
     * @var string
 *
* protected $_sThisTemplate = 'page/dre_recommendations/dre_recommlist.tpl';
     */

    /**
     * Collects current view data, return current template file name
     *
     * @return string
     */
    public function render()
    {
        return parent::render();
    }

    /**
     * Returns Bread Crumb - you are here page1/page2/page3...
     *
     * @return array
     */
    public function getBreadCrumb()
    {
        $paths = [];

        if ('oxmore' === \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('cnid')) {
            $path = [];
            $path['title'] = \OxidEsales\Eshop\Core\Registry::getLang()->translateString(
                'CATEGORY_OVERVIEW',
                \OxidEsales\Eshop\Core\Registry::getLang()->getBaseLanguage(),
                false
            );
            $path['link'] = $this->getLink();
            $categoryPath['welt'] = 'oxmore is set';


            $paths[] = $path;

            return $paths;
        }

        if (($categoryTree = $this->getCategoryTree()) && ($categoryPaths = $categoryTree->getPath())) {
            foreach ($categoryPaths as $category) {
                /** @var \OxidEsales\Eshop\Application\Model\Category $category */
                $categoryPath = [];

                $categoryPath['link'] = $category->getLink();
                $categoryPath['title'] = $category->oxcategories__oxtitle->value;
                $categoryPath['welt'] = 'welt'.$category->oxcategories__welt->value;

                $paths[] = $categoryPath;
            }
        }

        return $paths;
    }

    public function getWeltString($welt = 0){
        if($welt !== 0){
            return 'welt' . $welt;
        }else {
            if ($this->_isActCategory()) {
                $actCat = $this->getActiveCategory();
                $welt = $actCat->oxcategories__welt->value;
                return 'welt' . $welt;
            } else {
                return 0;
            }
        }
    }
}