<?php
// \OxidEsales\Eshop\Application\Controller\FrontendController => oxUBase

namespace Bender\dre_BodyConnect\Application\Controller;


class bnversand extends \OxidEsales\Eshop\Application\Controller\FrontendController {

    protected $_sClass = 'bnversand';
    /**
     * Current class template name.
     *
     * @var string
     */
    protected $_sThisTemplate = 'versand.tpl';

    /**
     * @var array
     */
    protected $arrCountyVersandsetAnzeige = array(
        'Deutschland'       => 'a7c40f631fc920687.20179984',
        'Österreich'        => 'a7c40f6320aeb2ec2.72885259',
        //'Belgien'           => 'a7c40f632e04633c9.47194042'
    );

    /**
     * @return null
     */
    public function render(){
        return parent::render();
    }

    /**
     * @return \OxidEsales\Eshop\Application\Model\CountryList
     */
    public function getCountryList(){

        $countrylist = oxNew(\OxidEsales\Eshop\Application\Model\CountryList::class);
        $sViewName = getViewName('oxcountry');
        $sSelect = "SELECT oxid, oxtitle, oxisoalpha2, oxvatstatus FROM {$sViewName} WHERE oxactive = '1' order by oxorder";
        $countrylist->selectString($sSelect);

        return $countrylist;
    }

    /**
     * @param $oxid
     * @return bool
     */
    public function isCountrySetToShowVersandset($oxid){
        if(\in_array($oxid, $this->arrCountyVersandsetAnzeige)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $landid
     * @return array
     */
    public function getDeliveryList($landid){

        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        #$deliverylist = array();

        $sSelect = '
                SELECT
                    `oxobject2delivery`.`oxdeliveryid` 
                FROM `oxobject2delivery`
                WHERE `oxobject2delivery`.`oxobjectid` = "' . $landid . '"
                    AND `oxobject2delivery`.`oxtype` = "oxcountry"';

        //return array($oDb->getAll($sSelect));// getAll($sSelect);
        $rs = $oDb->getCol($sSelect); // getCol($sSelect);   //getCol($sSelect);
        foreach($rs as $arr){
            if(!empty($arr)){
                $deliverylist[] = $arr;
            }
        }


        return $deliverylist;
    }

    /**
     * @param $oxid
     * @return \OxidEsales\Eshop\Application\Model\Delivery
     */
    public function getDelivery($oxid){

        $oDelSet = oxNew(\OxidEsales\Eshop\Application\Model\Delivery::class);
        $oDelSet->load($oxid);

        if($oDelSet->isLoaded()){
            return $oDelSet;
        }else{
            return false;
        }



    }

    /**
     * @param $landid
     * @return array
     */
    public function getDeliverySets($landid){
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();

        $sSelect = '
                SELECT
                    `oxobject2delivery`.`oxdeliveryid` 
                FROM `oxobject2delivery`
                WHERE `oxobject2delivery`.`oxobjectid` = "' . $landid . '"
                    AND `oxobject2delivery`.`oxtype` = "oxcountry"';

        $rs = $oDb->getCol($sSelect);//getAll($sSelect); //  getCol($sSelect);
        //return $rs;
        //$deliverylist[] = $rs;
        //$versandsets = null;

        foreach($rs as $id){
            //return $id;
            $select = "select * from `oxdelivery` where `OXID` = '".$id."' and OXACTIVE = 1 and VERSANDANZEIGE = 0 order by OXSORT limit 0,100";
            //return $select;
            $dies = $oDb->getAll($select); // getArray($select)[0];
            //return $dies;
            if($dies[0] !== null){
                $versandsets[] = $dies[0];
            }
        }
        //return $versandsets;
        usort($versandsets, function ($a, $b) {
            return strcmp($a[15], $b[15]);
        });

        //return $this->sksort($versandsets);
        return $versandsets;

    }

    /**
     * @param $deliverioxid
     * @return array
     */
    public function getCategories($deliverioxid){
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb($fetchMode = \OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_ASSOC);

        $sSelect = '
                SELECT
                    `oxobject2delivery`.`oxobjectid` 
                FROM `oxobject2delivery`
                WHERE `oxobject2delivery`.`oxdeliveryid` = "' . $deliverioxid . '"
                    AND `oxobject2delivery`.`oxtype` = "oxcategories"';

        //return $sSelect;
        $rs = $oDb->getAll($sSelect); // getArray($sSelect);
        //return $rs;
        $categories = array();

        foreach($rs as $id){
            //return $id;
            $select = 'select * from `oxcategories` where `OXID` = \''.$id['oxobjectid'].'\' and OXACTIVE = 1 and AusblendenVersand = 0  order by OXSORT limit 0,100';
            //return $select;
            $dies = $oDb->getAll($select); // getArray($select);
            //return $dies;
            if(!empty($dies)){
                #echo $dies[0][0];

                $categories[] = $dies[0];
                    //return $dies;
            }
        }
        /*
        usort($categories, function ($a, $b) {
            return strcmp($a['OXSORT'], $b['OXSORT']);
        });
        */

        return $categories;
        if(is_array($categories) && !empty($categories)){
            return $this->sksort($categories);
        }else{
            return $categories;
        }
        #return $this->sksort($categories);
        #echo '<pre>';
        #print_r($categories);
        #print_r($dies[0][0]);
        #die();
        #return $categories;
    }

    /**
     * @param $oxid
     * @return oxCategory
     */
    public function getCategoryObject($oxid){
        $catobj = oxNew(\OxidEsales\Eshop\Application\Model\Category::class);
        $catobj->load($oxid);

        return $catobj;
    }

    /**
     * @param $array
     * @param int $subkey
     * @param bool $sort_ascending
     * @return array
     */
    public function sksort(&$array, $subkey=5, $sort_ascending=true) {

        $temp_array = null;
        if(\count($array)){
            $temp_array[key($array)] = array_shift($array);
        }

        foreach($array as $key => $val){
            $offset = 0;
            $found = false;
            foreach($temp_array as $tmp_key => $tmp_val){

                if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])){

                    $temp_array = array_merge(\array_slice($temp_array,0,$offset),
                        array($key => $val),
                        \array_slice($temp_array,$offset)
                    );
                    $found = true;
                }
                $offset++;
            }
            if(!$found){
                $temp_array = array_merge($temp_array, array($key => $val));
            }
        }

        if($sort_ascending){
            $array = array_reverse($temp_array);
        } else {
            $array = $temp_array;
        }
        return $array;
    }
}










