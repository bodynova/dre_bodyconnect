<?php

namespace Bender\dre_BodyConnect\Application\Controller;

class dre_articledetailscontroller extends \OxidEsales\Eshop\Application\Controller\ArticleDetailsController{

    protected $_sThisTemplate = 'detailsmin.tpl';

    protected $_sClass = 'dredetails';

    public function render(){

        parent::render();
        return $this->_sThisTemplate;
    }
}