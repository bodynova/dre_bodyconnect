<?php

namespace Bender\dre_BodyConnect\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Component;
use OxidEsales\Eshop\Core\ShopVersion;
use OxidEsales\Eshop\Core\Request;


class dre_article_main extends dre_article_main_parent
{
    public function render(){
        parent::render();
        return 'dre_article_main.tpl';
    }
}