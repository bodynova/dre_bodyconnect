<?php

namespace Bender\dre_BodyConnect\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Component;

class dre_category_list extends \OxidEsales\Eshop\Application\Controller\Admin\CategoryList
{
	public function render() {


		$template = parent::render();
        return 'dre_category_list.tpl';
		/*
		$user = oxNew(\OxidEsales\Eshop\Application\Component\UserComponent::class);
		$oUser = $user->getUser();

		if ($oUser->getId() === 'oxdefaultadmin' || \OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam('blDreArShowArticleOwner') == 1){
			return 'dre_article_list.tpl';
		}
		*/
		//return $template;
	}

	/**
     * Builds and returns array of SQL WHERE conditions.
     *
     * @return array
     */
    public function buildWhere()
    {
        /*
        // we override this to select only parent articles
        $this->_aWhere = parent::buildWhere();

        $user = oxNew(\OxidEsales\Eshop\Application\Component\UserComponent::class);
        $oUser = $user->getUser();

        // adding folder check
        $sFolder = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('folder');

        if ($sFolder && $sFolder != '-1' && ($adminrights = $oUser->oxuser__drerestrict->value) && strpos($adminrights, 'noforeignart') !== false ) {
            $this->_aWhere[getViewName("oxarticles") . ".dreuserid"] = $sFolder;
            //$this->_aWhere[getViewName("oxarticles") . ".oxfolder"] = $sFolder;
        }
        */

        return $this->_aWhere;
    }
}
