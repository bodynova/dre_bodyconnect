<?php

namespace Bender\dre_BodyConnect\Application\Controller\Admin;

use \OxidEsales\Eshop\Core\Registry;

class dre_content_main extends dre_content_main_parent //dre_Content_Main_parent
{

    public function render(){
        $return = parent::render();
        return $return;
    }

    public function save()
    {
        parent::save();
        $this->resetContentCache();

        $myConfig = $this->getConfig();

        $soxId   = $this->getEditObjectId();
        //$aParams = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter("editval");
        $aParams = \OxidEsales\Eshop\Core\Request::getRequestEscapedParameter("editval");

        //die($aParams);
        // FixSmartyInContent Start
        if (isset($aParams['oxcontents__oxcontent'])) {
            $aParams['oxcontents__oxcontent'] = str_replace('-&gt;', '->', $aParams['oxcontents__oxcontent']);
        }
        // FixSmartyInContent Ende

        if (isset($aParams['oxcontents__oxloadid'])) {
            $aParams['oxcontents__oxloadid'] = $this->_prepareIdent($aParams['oxcontents__oxloadid']);
        }

        // check if loadid is unique
        if ($this->_checkIdent($aParams['oxcontents__oxloadid'], $soxId)) {
            // loadid already used, display error message
            $this->_aViewData["blLoadError"] = true;

            $oContent = oxNew(\OxidEsales\Eshop\Application\Model\Content::class);
            if ($soxId != '-1') {
                $oContent->load($soxId);
            }
            $oContent->assign($aParams);
            $this->_aViewData["edit"] = $oContent;

            return;
        }

        // checkbox handling
        if (!isset($aParams['oxcontents__oxactive'])) {
            $aParams['oxcontents__oxactive'] = 0;
        }

        // special treatment
        if ($aParams['oxcontents__oxtype'] == 0) {
            $aParams['oxcontents__oxsnippet'] = 1;
        } else {
            $aParams['oxcontents__oxsnippet'] = 0;
        }

        //Updates object folder parameters
        if ($aParams['oxcontents__oxfolder'] == 'CMSFOLDER_NONE') {
            $aParams['oxcontents__oxfolder'] = '';
        }

        $oContent = oxNew(\OxidEsales\Eshop\Application\Model\Content::class);

        if ($soxId != "-1") {
            $oContent->loadInLang($this->_iEditLang, $soxId);
        } else {
            $aParams['oxcontents__oxid'] = null;
        }

        //$aParams = $oContent->ConvertNameArray2Idx( $aParams);

        $oContent->setLanguage(0);
        $oContent->assign($aParams);
        $oContent->setLanguage($this->_iEditLang);
        $oContent->save();

        // set oxid if inserted
        $this->setEditObjectId($oContent->getId());
    }

}