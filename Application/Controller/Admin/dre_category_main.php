<?php

namespace Bender\dre_BodyConnect\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Component;
use OxidEsales\Eshop\Core\ShopVersion;
use OxidEsales\Eshop\Core\Request;


class dre_category_main extends dre_category_main_parent
{
    
    protected $arMyParams = null;
    
    public function render(){

        /*
        $requirements = $this->checkRequirements();
        //$pRender = parent::render();
        $user = oxNew(\OxidEsales\Eshop\Application\Component\UserComponent::class);
        $oUser = $user->getUser();
        $arUserRights = array();
        $soxId = $this->getEditObjectId();
        $userid = $oUser->getId();

        if ($sUserRights = $oUser->oxuser__drerestrict->value) {
            $arUserRights = explode(',', $sUserRights);
        }

        $oArticle = $this->createArticle();
        $oArticle->setLanguage($this->_iEditLang);

        if ($soxId != "-1") {
            $oArticle->loadInLang($this->_iEditLang, $soxId);
        }

        if(in_array('noforeignart', $arUserRights , true) && $oArticle->oxarticles__dreuserid->value != $userid ) {
            $this->_aViewData['readonly'] = true;
        }

        /**
         * debugbereich
         */
        /*
        $debug['noforeignart'] = in_array('noforeignart', $arUserRights , true);
        $debug['dre_userid'] = $oArticle->oxarticles__dreuserid->value;
        $debug['$userid'] = $userid;
        $debug['_aViewData'] = $this->_aViewData;

        $logger = \OxidEsales\Eshop\Core\Registry::getLogger();
        $logger->debug('debug', $debug);
        */
        return parent::render();


        if (\OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter("aoc")) {
            /** @var \OxidEsales\Eshop\Application\Controller\Admin\CategoryMainAjax $oCategoryMainAjax */
            $oCategoryMainAjax = oxNew(\OxidEsales\Eshop\Application\Controller\Admin\CategoryMainAjax::class);
            $this->_aViewData['oxajax'] = $oCategoryMainAjax->getColumns();

            return "popups/category_main.tpl";
        }


        return "dre_category_main.tpl";
    }

    /*
        protected function checkRequirements()
        {


            $shopversion = oxNew( ShopVersion::class )->getVersion();

            if(version_compare( $shopversion , '4.5.0', '>=')){
                return $this->getEditObjectId();
            }

            if (null === ( $sEditOxid = $this->_sEditObjectId )) {
                if ( null === ( $sEditOxid = Registry::get( Request::class )->getRequestEscapedParameter('oxid'))) {
                    $sEditOxid = Registry::getSession()->getVariable('saved_oxid');
                }
            }

            return $sEditOxid;

        } */
    /*
        public function addDefaultValues($aParams)
        {

            $user = oxNew(\OxidEsales\Eshop\Application\Component\UserComponent::class);
            $oUser = $user->getUser();
            $adminRights = null;
            $sUserId = $oUser->getId();


            return $aParams;
        }*/
}
