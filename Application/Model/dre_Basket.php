<?php

namespace Bender\dre_BodyConnect\Application\Model;

use OxidEsales\Eshop\Core\Registry;


class dre_Basket extends dre_Basket_parent
{
    protected $usedDiscounts;
    protected $_oNotDiscountedProductsPriceListVoucher;
    protected $voucherImmerAnwenden;


    public function getVoucherAnwenden()
    {
        return $this->voucherImmerAnwenden;
    }

    public function setVoucherAnwenden(){
        $this->voucherImmerAnwenden = true;
    }

    public function getUsedDiscounts()
    {
        return $this->usedDiscounts;
    }

    public function findDeliveryCountry(){
        $myConfig = $this->getConfig();
        $oUser = $this->getBasketUser();

        //return $oUser;
        $sDeliveryCountry = null;


        if (!$oUser) {
            // don't calculate if not logged in unless specified otherwise
            $aHomeCountry = $myConfig->getConfigParam('aHomeCountry');
            if ($myConfig->getConfigParam('blCalculateDelCostIfNotLoggedIn') && is_array($aHomeCountry)) {
                $sDeliveryCountry = current($aHomeCountry);
            }
        } else {
            // ok, logged in
            if ($sCountryId = $myConfig->getGlobalParameter('delcountryid')) {
                $sDeliveryCountry = $sCountryId;
            } elseif ($sAddressId = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable('deladrid')) {
                $oDeliveryAddress = oxNew(\OxidEsales\Eshop\Application\Model\Address::class);
                if ($oDeliveryAddress->load($sAddressId)) {
                    $sDeliveryCountry = $oDeliveryAddress->oxaddress__oxcountryid->value;
                }
            }

            // still not found ?
            if (!$sDeliveryCountry) {
                $sDeliveryCountry = $oUser->oxuser__oxcountryid->value;
            }
        }

        return $sDeliveryCountry;
    }

    /**
     * Checks and sets voucher information. Checks it's availability according
     * to few conditions: oxvoucher::checkVoucherAvailability(),
     * oxvoucher::checkUserAvailability(). Errors are stored in
     * \OxidEsales\Eshop\Application\Model\Basket::voucherErrors array. After all voucher is marked as reserved
     * (oxvoucher::MarkAsReserved())
     *
     * @param string $sVoucherId voucher ID
     */
    public function addVoucher($sVoucherId)
    {
        // calculating price to check
        // P using prices sum which has discount, not sum of skipped discounts
        $dPrice = 0;
        if ($this->_oDiscountProductsPriceList) {
            $dPrice = $this->_oDiscountProductsPriceList->getSum($this->isCalculationModeNetto());
        }

        try { // trying to load voucher and apply it
            $oVoucher = oxNew(\OxidEsales\Eshop\Application\Model\Voucher::class);

            if (!$this->_blSkipVouchersAvailabilityChecking) {
                $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getMaster();

                $oDb->startTransaction();

                try {
                    $oVoucher->getVoucherByNr($sVoucherId, $this->_aVouchers, true);
                    $oSeries = $oVoucher->getSerie();
                    if ($oSeries->oxvoucherseries__oxnoskipdiscount->value) {
                        $this->setVoucherAnwenden();
                        if($this->_oNotDiscountedProductsPriceListVoucher->getSum($this->isCalculationModeNetto()) > 0){
                            $dPrice =  $this->_oNotDiscountedProductsPriceListVoucher->getSum($this->isCalculationModeNetto());
                        }else{
                            $dPrice = $this->_oNotDiscountedProductsPriceList->getSum($this->isCalculationModeNetto());
                        }
                    }else{
                        $dPrice = $this->_oDiscountProductsPriceList->getSum($this->isCalculationModeNetto());
                    }
                    $oVoucher->checkVoucherAvailability($this->_aVouchers, $dPrice);
                    $oVoucher->checkUserAvailability($this->getBasketUser());
                    $oVoucher->markAsReserved();
                } catch (\Exception $exception) {
                    $oDb->rollbackTransaction();

                    if ($exception instanceof \OxidEsales\Eshop\Core\Exception\VoucherException) {
                        throw $exception;
                    } else {
                        $oEx = oxNew(\OxidEsales\Eshop\Core\Exception\VoucherException::class);
                        $oEx->setMessage('Something went wrong, please try again');
                        $oEx->setVoucherNr($oVoucher->oxvouchers__oxvouchernr->value);
                        throw $oEx;
                    }
                }

                $oDb->commitTransaction();
            } else {
                $oVoucher->load($sVoucherId);
            }

            // saving voucher info
            $this->_aVouchers[$oVoucher->oxvouchers__oxid->value] = $oVoucher->getSimpleVoucher();
        } catch (\OxidEsales\Eshop\Core\Exception\VoucherException $oEx) {
            // problems adding voucher
            \OxidEsales\Eshop\Core\Registry::getUtilsView()->addErrorToDisplay($oEx, false, true);
        }

        $this->onUpdate();
    }

    /**
     * Removes voucher from basket and unreserved it.
     *
     * @param string $sVoucherId removable voucher ID
     */
    public function removeVoucher($sVoucherId)
    {
        // removing if it exists
        if (isset($this->_aVouchers[$sVoucherId])) {
            $oVoucher = oxNew(\OxidEsales\Eshop\Application\Model\Voucher::class);
            $oVoucher->load($sVoucherId);

            $oVoucher->unMarkAsReserved();

            // unset it if exists this voucher in DB or not
            unset($this->_aVouchers[$sVoucherId]);
            $this->onUpdate();
        }
    }

    /**
     * Calculates voucher discount
     * @deprecated underscore prefix violates PSR12, will be renamed to "calcVoucherDiscount" in next major
     */
    protected function _calcVoucherDiscount() // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    {
        if ($this->getConfig()->getConfigParam('bl_showVouchers') && ($this->_oVoucherDiscount === null || ($this->_blUpdateNeeded && !$this->isAdmin()))) {
            $this->_oVoucherDiscount = $this->_getPriceObject();

            // calculating price to apply discount
            $dPrice = $this->_oDiscountProductsPriceList->getSum($this->isCalculationModeNetto()) - $this->_oTotalDiscount->getPrice();

            if($this->getVoucherAnwenden()){
                if ($this->_oNotDiscountedProductsPriceListVoucher->getSum($this->isCalculationModeNetto()) > 0) {
                    $dPrice = $this->_oNotDiscountedProductsPriceListVoucher->getSum($this->isCalculationModeNetto());
                } else {
                    $dPrice = $this->_oNotDiscountedProductsPriceList->getSum($this->isCalculationModeNetto());
                }
                $dPrice = $dPrice - $this->_oTotalDiscount->getPrice();
            }else{
                $dPrice = $this->_oDiscountProductsPriceList->getSum($this->isCalculationModeNetto()) - $this->_oTotalDiscount->getPrice();
            }

            /*
            echo '<pre>';
            print_r($this->_oNotDiscountedProductsPriceListVoucher);
            echo '</pre>';
            echo $dPrice;
            */

            // recalculating
            if (count($this->_aVouchers)) {
                $oLang = \OxidEsales\Eshop\Core\Registry::getLang();
                foreach ($this->_aVouchers as $sVoucherId => $oStdVoucher) {
                    $oVoucher = oxNew(\OxidEsales\Eshop\Application\Model\Voucher::class);
                    try { // checking
                        $oVoucher->load($oStdVoucher->sVoucherId);

                        if (!$this->_blSkipVouchersAvailabilityChecking) {
                            $oVoucher->checkBasketVoucherAvailability($this->_aVouchers, $dPrice);
                            $oVoucher->checkUserAvailability($this->getBasketUser());
                        }

                        // assigning real voucher discount value as this is the only place where real value is calculated
                        $dVoucherdiscount = $oVoucher->getDiscountValue($dPrice);

                        if ($dVoucherdiscount > 0) {
                            $dVatPart = ($dPrice - $dVoucherdiscount) / $dPrice * 100;

                            if (!$this->_aDiscountedVats) {
                                if ($oPriceList = $this->getDiscountProductsPrice()) {
                                    $this->_aDiscountedVats = $oPriceList->getVatInfo($this->isCalculationModeNetto());
                                }
                            }

                            // apply discount to vat
                            foreach ($this->_aDiscountedVats as $sKey => $dVat) {
                                $this->_aDiscountedVats[$sKey] = \OxidEsales\Eshop\Core\Price::percent($dVat, $dVatPart);
                            }
                        }

                        // accumulating discount value
                        $this->_oVoucherDiscount->add($dVoucherdiscount);

                        // collecting formatted for preview
                        $oStdVoucher->fVoucherdiscount = $oLang->formatCurrency($dVoucherdiscount, $this->getBasketCurrency());
                        $oStdVoucher->dVoucherdiscount = $dVoucherdiscount;

                        // subtracting voucher discount
                        $dPrice = $dPrice - $dVoucherdiscount;
                    } catch (\OxidEsales\Eshop\Core\Exception\VoucherException $oEx) {
                        // removing voucher on error
                        $oVoucher->unMarkAsReserved();
                        unset($this->_aVouchers[$sVoucherId]);

                        // storing voucher error info
                        \OxidEsales\Eshop\Core\Registry::getUtilsView()->addErrorToDisplay($oEx, false, true);
                    }
                }
            }
        }
    }

    protected function _calcItemsPrice()
    {
        /*
        echo '<pre>';
        print_r($this->_oNotDiscountedProductsPriceList->getSum($this->isCalculationModeNetto()));
        print_r($this->_oDiscountProductsPriceList->getSum($this->isCalculationModeNetto()));
        print_r($this->getVouchers());
        echo '</pre>';
        */
        //
        //parent::_calcItemsPrice();
        //start Parent
        // resetting
        $this->setSkipDiscounts(false);
        $this->_iProductsCnt = 0; // count different types
        $this->_dItemsCnt = 0; // count of item units
        $this->_dWeight = 0; // basket weight

        $this->_oProductsPriceList = oxNew(\OxidEsales\Eshop\Core\PriceList::class);
        $this->_oDiscountProductsPriceList = oxNew(\OxidEsales\Eshop\Core\PriceList::class);
        $this->_oNotDiscountedProductsPriceList = oxNew(\OxidEsales\Eshop\Core\PriceList::class);
        $this->_oNotDiscountedProductsPriceListVoucher = oxNew(\OxidEsales\Eshop\Core\PriceList::class);

        $oDiscountList = \OxidEsales\Eshop\Core\Registry::get(\OxidEsales\Eshop\Application\Model\DiscountList::class);

        if(count($this->_aVouchers)){
            foreach($this->_aVouchers as $voucher){
                $oVoucher = oxNew(\OxidEsales\Eshop\Application\Model\Voucher::class);
                $oVoucher->load($voucher->sVoucherId);
                $oSeries = $oVoucher->getSerie();
                if ($oSeries->oxvoucherseries__oxnoskipdiscount->value) {
                    $this->setVoucherAnwenden();
                }
            }
        }


        /** @var \oxBasketItem $oBasketItem */
        foreach ($this->_aBasketContents as $oBasketItem) {
            $this->_iProductsCnt++;
            $this->_dItemsCnt += $oBasketItem->getAmount();
            $this->_dWeight += $oBasketItem->getWeight();

            if (!$oBasketItem->isDiscountArticle() && ($oArticle = $oBasketItem->getArticle(true))) {
                $oBasketPrice = $oArticle->getBasketPrice($oBasketItem->getAmount(), $oBasketItem->getSelList(), $this);
                $oBasketItem->setRegularUnitPrice(clone $oBasketPrice);

                if (!$oArticle->skipDiscounts() && $this->canCalcDiscounts()) {
                    // apply basket type discounts for item
                    $aDiscounts = $oDiscountList->getBasketItemDiscounts($oArticle, $this, $this->getBasketUser());
                    reset($aDiscounts);
                    /** @var \oxDiscount $oDiscount */
                    foreach ($aDiscounts as $oDiscount) {
                        $oBasketPrice->setDiscount($oDiscount->getAddSum(), $oDiscount->getAddSumType());
                    }
                    $oBasketPrice->calculateDiscount();
                } else {
                    $oBasketItem->setSkipDiscounts(true);
                    $this->setSkipDiscounts(true);
                }

                $oBasketItem->setPrice($oBasketPrice);
                $this->_oProductsPriceList->addToPriceList($oBasketItem->getPrice());

                //P collect discount values for basket items which are discountable
                if (!$oArticle->skipDiscounts()) {
                    $this->_oDiscountProductsPriceList->addToPriceList($oBasketItem->getPrice());
                    $this->_oNotDiscountedProductsPriceListVoucher->addToPriceList($oBasketItem->getPrice());
                } else {
                    $this->_oNotDiscountedProductsPriceList->addToPriceList($oBasketItem->getPrice());
                    $this->_oNotDiscountedProductsPriceListVoucher->addToPriceList($oBasketItem->getPrice());
                    $oBasketItem->setSkipDiscounts(true);
                    $this->setSkipDiscounts(true);
                }
            } elseif ($oBasketItem->isBundle()) {
                // if bundles price is set to zero
                $oPrice = oxNew(\OxidEsales\Eshop\Core\Price::class);
                $oBasketItem->setPrice($oPrice);
            }
        }

        // ende Parent
        $oDiscountList = Registry::get("oxDiscountList");



        foreach ($this->_aBasketContents as $oBasketItem) {

            if (!$oBasketItem->isDiscountArticle() && ($oArticle = $oBasketItem->getArticle(true))) {

                $oBasketPrice = $oArticle->getBasketPrice($oBasketItem->getAmount(), $oBasketItem->getSelList(), $this);

                if (!$oArticle->skipDiscounts() && $this->canCalcDiscounts()) {
                    // apply basket type discounts for item
                    $aDiscounts = $oDiscountList->getBasketItemDiscounts($oArticle, $this, $this->getBasketUser());
                    //
                    reset($aDiscounts);
                    $oBasketItem->clearUsedDiscount();


                    foreach ($aDiscounts as $oDiscount) {
                        // kopiere original objkt
                        $oCopyBasketPrice = clone $oBasketPrice;

                        // wende rabatt an
                        $oBasketPrice->setDiscount($oDiscount->getAddSum(), $oDiscount->getAddSumType());

                        $oBasketPrice->calculateDiscount();

                        #print_r( $oBasketPrice);
                        #print_r($test);
                        // den benutzen discount wert mit namen des rabatts an den item im basket speichern
                        /*$oBasketItem->addUsedDiscount(
                            $oDiscount->oxdiscount__oxtitle->rawValue,
                            ($oCopyBasketPrice->getPrice() - $oBasketPrice->getPrice())
                        );*/
                        $oBasketItem->addUsedDiscount(
                            $oDiscount->oxdiscount__oxtitle->rawValue,
                            ($oBasketItem->getPrice()->dPriceBeforeStaffel - $oBasketPrice->getPrice())
                        );

                        #$this->debug_to_console($oBasketItem);

                        //test
                        #$this->_oDiscountProductsPriceList->addToPriceList($oBasketItem->getPrice());
                        #echo '$oBasketItem: ';
                        #var_dump($oBasketItem->getPrice()->dPriceBeforeStaffel);
                        #var_dump($oBasketPrice->getPrice());
                        #echo ($oCopyBasketPrice->getPrice() - $oBasketPrice->getPrice());*/
                    }
                    /*
                    $oBasketItem->setPrice($oBasketPrice);
                    $this->_oProductsPriceList->addToPriceList($oBasketItem->getPrice());

                    //P collect discount values for basket items which are discountable
                    if (!$oArticle->skipDiscounts()) {

                        $this->_oDiscountProductsPriceList->addToPriceList($oBasketItem->getPrice());
                    } else {
                        $this->_oNotDiscountedProductsPriceList->addToPriceList($oBasketItem->getPrice());
                        $oBasketItem->setSkipDiscounts(true);
                        $this->setSkipDiscounts(true);
                    }*/
                }
            }

        }
    }

    public function getUsedDiscountsAsGroup()
    {
        $arrUsedDiscounts = array('Staffelpreis'=>0);
        //$arrUsedDiscounts = array();
        #print_r($arrUsedDiscounts);
        foreach ($this->_aBasketContents AS $oBasketItem) {

            #print_r($oBasketItem->arrUsedDiscounts);
            #print_r(count($oBasketItem->arrUsedDiscounts));
            if (count($oBasketItem->arrUsedDiscounts)) {
                foreach ($oBasketItem->arrUsedDiscounts AS $strName => $dValue) {
                    // initalisieren mit 0
                    if (!isset($arrUsedDiscounts[$strName]))
                        $arrUsedDiscounts[$strName] = 0;
                    //
                    $arrUsedDiscounts[$strName] += $dValue; //* $oBasketItem->getAmount();
                    // ende
                    #echo $dValue;
                }

                #print_r($oBasketItem->arrUsedDiscounts);
                #die();

            } else {
                //
                $dStaffelMultiplyed = $oBasketItem->getPrice()->dPriceBeforeStaffel * $oBasketItem->getAmount();

                $dBrutto = $this->isCalculationModeNetto() ? $oBasketItem->getPrice()->getNettoPrice() : $oBasketItem->getPrice()->getPrice();
                //
                if ($dStaffelMultiplyed != $dBrutto && ($dStaffelMultiplyed - $dBrutto) > 0) {
                    $arrUsedDiscounts['Staffelpreis'] += $dStaffelMultiplyed - $dBrutto;
                }
            }
        }
        #print_r($arrUsedDiscounts);
        #die;
        $this->usedDiscounts = $arrUsedDiscounts;
        return $arrUsedDiscounts;
    }

    public function testArticle($article)
    {

        $odiscount = oxnew('oxdiscount');

        $ret = $odiscount->_isArticleAssigned($article);

        return $ret;
    }

    public function getSumRabattBrutto()
    {
        $sumrabatt = 0;
        if (isset($this->usedDiscounts)) {
            foreach ($this->usedDiscounts as $discount) {
                $sumrabatt += $discount;
            }
        }
        return $sumrabatt;
    }

    public function getSumRabattNetto()
    {
        $sumrabatt = 0;
        if (isset($this->usedDiscounts)) {
            foreach ($this->usedDiscounts as $discount) {
                $sumrabatt += $discount;
            }
            $sumrabatt = $sumrabatt /1.16; //* (100 - 16) / 100;
        }
        return $sumrabatt;
    }

    public function getBruttoSumUnrabattiert()
    {
        $bruttosum = parent::getBruttoSum();

        $erg = $bruttosum + $this->getSumRabattBrutto();
        return $erg;
    }

    public function getProductsNetPriceUnrabattiert()
    {
        parent::_calcItemsPrice();
        $nettorabattiert = parent::getProductsNetPrice();
        $erg = $nettorabattiert + $this->getSumRabattNetto();
        return $erg;
    }
    public function getBruttoSum()
    {
        /* ORIGINAL:*/

        return parent::getBruttoSum();
        $dBrutto = parent::getBruttoSum();

        /*
        echo '<pre>';
        print_r($this);
        die();
        */

        foreach ($this->_aBasketContents AS $oBasketItem) {
            if (count($oBasketItem->arrUsedDiscounts)) {
                foreach ($oBasketItem->arrUsedDiscounts AS $strName => $dValue) {
                    $dBrutto += $dValue;// * $oBasketItem->getAmount();
                }
            } else {
                // old hat keine Auswahllisten berücksichtigt. : $dStaffelMultiplyed = $oBasketItem->getPrice()->dPriceBeforeStaffel * $oBasketItem->getAmount();
                $dStaffelMultiplyed = $oBasketItem->getPrice()->dPriceBeforeStaffel * $oBasketItem->getAmount();
                $dTmpBrutto = $oBasketItem->getPrice()->getPrice();
                //
                if ($dStaffelMultiplyed != $dTmpBrutto) {
                    $dBrutto += $dStaffelMultiplyed - $dTmpBrutto;
                }
            }
        }

        return $dBrutto;
        /*

         $dBrutto = parent::getBruttoSum();
         /*
         echo '<pre>';
         #print_r($this->_aBasketContents);
         foreach($this->_aBasketContents AS $oBasketItem) {
             if(count($oBasketItem->arrUsedDiscounts)) {
                echo '<hr>USED DISCOUNTS: ';
                 print_r($oBasketItem->arrUsedDiscounts);
                 foreach($oBasketItem->arrUsedDiscounts AS $strName=>$dValue) {
                     $dBrutto += $dValue * $oBasketItem->getAmount();
                 }
                 echo '$dBrutto: '.$dBrutto.'<br>';
             } else {
                 echo'NIX:';
             }
         }
         die;
         foreach($this->_aBasketContents AS $oBasketItem) {
             if(count($oBasketItem->arrUsedDiscounts)) {
                 foreach($oBasketItem->arrUsedDiscounts AS $strName=>$dValue) {
                     $dBrutto += $dValue * $oBasketItem->getAmount();
                 }
             } else {
                 //
                 $dStaffelMultiplyed = $oBasketItem->getPrice()->dPriceBeforeStaffel * $oBasketItem->getAmount();
                 $dTmpBrutto = $oBasketItem->getPrice()->getPrice();
                 //
                 if($dStaffelMultiplyed != $dTmpBrutto) {
                     $dBrutto+= $dStaffelMultiplyed-$dTmpBrutto;
                 }
             }
         }

         return $dBrutto;
        */
    }


    public function getBasketContents()
    {
        return $this->_aBasketContents;
    }

    public function debug_to_console($data)
    {

        $output = '';

        if (is_array($data)) {
            $output .= "<script>console.warn( 'Debug Objects with Array.' ); console.log( '" . implode(',', $data) . "' );</script>";
        } else if (is_object($data)) {
            $data = var_export($data, TRUE);
            $data = explode("\n", $data);
            foreach ($data as $line) {
                if (trim($line)) {
                    $line = addslashes($line);
                    $output .= "console.log( '{$line}' );";
                }
            }
            $output = "<script>console.warn( 'Debug Objects with Object.' ); $output</script>";
        } else {
            $output .= "<script>console.log( 'Debug Objects: {$data}' );</script>";
        }

        echo $output;
    }

}
