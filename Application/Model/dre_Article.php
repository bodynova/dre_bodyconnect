<?php
namespace Bender\dre_BodyConnect\Application\Model;

use Bender\dre_BodyConnect\Core\dre_Price;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use OxidEsales\Eshop\Core\Registry;

class dre_Article extends dre_Article_parent{

	protected $liefertage = 3;
    /**
     * Returns true if article is not buyable
     *
     * @return bool
     */
    public function isBnNotBuyable()
    {
        if (date('Y-m-d', ${strtotime($this->oxarticles__oxdelivery->value) - (60 * 60 * 56) }) >= date('Y-m-d') ){
            return false;
        }
	    if ($this->oxarticles__oxprice->value > 200) {
		    return false;
	    }
        if ($this->oxarticles__bnflagbestand->value == 2) {
            return true;
        }
        return $this->_blNotBuyable;
    }

    /**
     * Checks if article is buyable.
     *
     * @return bool
     */
    public function isBnBuyable()
    {
        $lieferzeitsekunden = $this->liefertage * 24 * 60 * 60;
        /*
        $aktuellesDatum = date('Y-m-d');
	    $oxdelivery = $this->oxarticles__oxdelivery->value;
        $aktuellesDatumAlsMicrotime = strtotime(date('Y-m-d'));
        $oxdeliveryAlsMicrotime = strtotime($this->oxarticles__oxdelivery->value);
        $lieferdatum = date('Y-m-d', strtotime($this->oxarticles__oxdelivery->value));
        $datumOhneLiefersekunden = date('Y-m-d', ${strtotime($this->oxarticles__oxdelivery->value) - $lieferzeitsekunden});
        $vaterNotBuyable = $this->_blNotBuyableParent;
        $selbstNotBuyable = $this->_blNotBuyable;
        */

	    // create a log channel
	    #$log = new Logger('Artnr:64314at');
	    #$log->pushHandler(new StreamHandler(Registry::getConfig()->getLogsDir() . 'buyableError.log', Logger::ERROR));
	    #$log->error('Lieferzeitsekunden @ ' . $lieferzeitsekunden .' Aktuelles Datum: '. $aktuellesDatum . ' oxdelivery: ' . $oxdelivery  . ' $aktuellesDatumAlsMicrotime:  ' .  $aktuellesDatumAlsMicrotime . ' $oxdeliveryAlsMicrotime:  '. $oxdeliveryAlsMicrotime . ' $lieferdatum: ' .$lieferdatum
		#    .' $datumOhneLiefersekunden '. $datumOhneLiefersekunden .' $vaterNotBuyable: '. $vaterNotBuyable .' $selbstNotBuyable: '. $selbstNotBuyable
	    #);
        /**
         * Wenn das Feld oxdelivery gefüllt ist,
         * und oxdelivery größer als das aktuelle Datum
         */
        if ($this->oxarticles__oxdelivery->value !== '0000-00-00' && (date('Y-m-d', strtotime($this->oxarticles__oxdelivery->value)) >= date('Y-m-d'))) {
            /**
             * Wenn das oxdelivery Datum - Tage kleiner ist als das aktuelle Datum...
             * TRUE
             */
            $sek1 = strtotime($this->oxarticles__oxdelivery->value) - $lieferzeitsekunden;
            if (date('Y-m-d', $sek1) <= date('Y-m-d')) {
                return true;
            }
        }
        if($this->oxarticles__oxprice->value > 200){
        	return true;
        }
        /**
         * Wenn das Feld bnflagbestand auf 2 steht.
         * FALSE
         */
        if ($this->oxarticles__bnflagbestand->value == 2) {
            return false;
        }
        /**
         * Shop Standartüberprüfung:
         */
        return !($this->_blNotBuyableParent || $this->_blNotBuyable);
    }

    protected function _getVarMinPrice()
    {
        // lade den suffix für den aktuellen user
        $sPriceSufix = $this->_getUserPriceSufix();
        // wenn kein _dVarMinPrice geladen wurde und der user keiner gruppe (abc preis) angehört laden wir
        // den niedrigsten preis der ersten aktiven variante
        if ( $this->_dVarMinPrice === null && $sPriceSufix === '' )
        {
            //
            $sSql = 'SELECT OXPRICE ';
            $sSql .=  ' FROM ' . $this->getViewName(true) . '
                        WHERE ' .$this->getSqlActiveSnippet(true) . '
                            AND ( `oxparentid` = ' . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote( $this->getId() ) . ' )'.
                'ORDER By OXPRICE ASC;';
            //
            $this->_dVarMinPrice = \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->getOne( $sSql );
            #echo '<pre>';
            #print_r($sSql);
            #exit;
        } else {
            parent::_getVarMinPrice();
        }
        return $this->_dVarMinPrice;
    }

    /**
     *
     * Prozentuale Ersparnis berechnen
     *
     * @return string
     */
    public function getMySaving()
    {
        //define the variable you want to fill
        $sProductSaving = '';
        //Assign a default value for $dProductOldPrice in case there is no TPrice
        $dProductOldPrice = 0;
        //Grab the data you need via getters from the database and put them into a self defined variable
        //First get the old price of the product. Please note: from 4.7/5.0 it must be getPrice.
        //Check if there is any getTPrice first:
        $oTPrice = $this->getTPrice();

        /*
        echo '<pre>';
        print_r($oTPrice->getPrice());
        die();
        */
        if ($oTPrice) {
            $dProductOldPrice = $oTPrice->getPrice();
        }
        //If this old price is bigger than "0",
        if ($dProductOldPrice > 0) {
            //get the present price of the product,
            if($this->getPrice()->getPrice() == 0){
                $dProductPrice = $this->_getVarMinPrice();
            }else{
                $dProductPrice = $this->getPrice()->getPrice();
            }

            //calculate the difference between the old and the new price as a percentage and ...
            $dProductSaving = 100 - ($dProductPrice * 100 / $dProductOldPrice);
            if ($dProductSaving > 0) {
                //... format the output:
                $sProductSaving = number_format($dProductSaving, 0, "", "");
            }
        }
/*
        echo '<pre>';
        print_r($dProductPrice);
        die();
*/
        //Return the value of your calculation so it can be parsed to the template
        return $sProductSaving;
    }


    // $this->_modifySelectListPrice( $dBasePrice, $aSelList );

    protected $blContainsVariantPrice = false;


    public function getPrice( $dAmount = 1 ) {
        $ret =  parent::getPrice( $dAmount );



        if(!$this->blContainsVariantPrice && isset($_POST['sel'])) {
            $ret->setPrice($this->_modifySelectListPrice( $ret->getPrice(), $_POST['sel']));
            $this->blContainsVariantPrice = true;
        }


        return $ret;


    }


    /**
     * @var bool
     */
    protected $dPriceBeforeStaffel = null;

    /**
     * @param int $dAmount
     * @return mixed
     */
    protected function _getAmountPrice($dAmount = 1)
    {
        //
        $this->dPriceBeforeStaffel = $this->_getGroupPrice();
        return parent::_getAmountPrice($dAmount);
    }


    /**
     * @param null $blCalculationModeNetto
     * @return dre_bnoxprice
     */
    protected function _getPriceObject( $blCalculationModeNetto = null )
    {
        $oPrice = new dre_Price();
        $oPrice->dPriceBeforeStaffel =  $this->dPriceBeforeStaffel;

        if ( $blCalculationModeNetto === null ) {
            $blCalculationModeNetto = $this->_isPriceViewModeNetto();
        }

        if ( $blCalculationModeNetto ) {
            $oPrice->setNettoPriceMode();
        } else {
            $oPrice->setBruttoPriceMode();
        }

        return $oPrice;
    }



    /**
     * @param $dAmount
     * @param $aSelList
     * @param $oBasket
     * @return mixed
     */
    public function getBasketPrice($dAmount, $aSelList, $oBasket )
    {
        //return parent::getBasketPrice($dAmount, $aSelList, $oBasket);
        //
        $dTmpAmount = 0;
        //
        if($this->oxarticles__oxparentid->value) {
            // LADE MEINE vater
            $objParentArticle = $this->getParentArticle();
            $objParentArticle->setNoVariantLoading(false);

            // LADE DIE KINDER DIESER PARENITD
            $arrChildIds = array(); // dieses array mit den child oxidy füllen
            foreach($objParentArticle->getVariants() AS $objVariantArticle) {
                $arrChildIds[] = $objVariantArticle->getId();
            }
            // LADE WIEVIELE ARTIKEL VON MIR SELBST SIND IM BASET
            foreach($oBasket->getContents() AS $objBasketItem) {
                if(in_array($objBasketItem->getArticle()->getId(), $arrChildIds)) {
                    $dTmpAmount+=$objBasketItem->getAmount();
                }
            }
            // ende
        } else {
            $dTmpAmount = $dAmount;
        }
        //
        $oPrice = parent::getBasketPrice($dTmpAmount, $aSelList, $oBasket );

        // überprüfung ob auch die Auswahllisten an der PriceBeforeStaffel angewendet wurden

        /*
        if($this->dPriceBeforeStaffel < $oPrice->getPrice()){
            $oPrice->dPriceBeforeStaffel = $oPrice->getPrice();
        }else {
            $oPrice->dPriceBeforeStaffel = $this->dPriceBeforeStaffel;
        }
        */
        // alte PriceBefore Staffel ohne Überprüfung
         $oPrice->dPriceBeforeStaffel = $this->dPriceBeforeStaffel;


        //return $oPrice;

        if($this->getArticleVat()!=$oPrice->getVat())
        {
            $oPrice->dPriceBeforeStaffel = round(($oPrice->dPriceBeforeStaffel/(100 + $this->getArticleVat()))*100, 2);
        }

        //
        return $oPrice;
    }

    /**
     * Assigns parent field values to article
     *
     * @param string $sFieldName field name
     *
     * @return null;
     */
    protected function _assignParentFieldValue($sFieldName)
    {
        if (!($oParentArticle = $this->getParentArticle())) {
            return;
        }

        $sCopyFieldName = $this->_getFieldLongName($sFieldName);

        // assigning only these which parent article has
        if ( $oParentArticle->$sCopyFieldName != null ) {

            // only overwrite database values
            if ( substr( $sCopyFieldName, 0, 12) != 'oxarticles__') {
                return;
            }

            //do not copy certain fields
            if (in_array($sCopyFieldName, $this->_aNonCopyParentFields)) {
                return;
            }

            //skip picture parent value assignment in case master image is set for variant
            /*if ($this->_isFieldEmpty($sCopyFieldName) && $this->_isImageField($sCopyFieldName) && $this->_hasMasterImage( 1 )) {
                return;
            }*/

            //COPY THE VALUE
            if ( $this->_isFieldEmpty($sCopyFieldName) ) {
                $this->$sCopyFieldName = clone $oParentArticle->$sCopyFieldName;
            }
        }
    }

    /**
     * Returns variants selections lists array
     *
     * @param array  $aFilterIds    ids of active selections [optional]
     * @param string $sActVariantId active variant id [optional]
     * @param int    $iLimit        limit variant lists count (if non zero, return limited number of multidimensional variant selections)
     *
     * @return array
     */
    /*
    public function getVariantSelections( $aFilterIds = null, $sActVariantId = null, $iLimit = 0 )
    {
        $iLimit = (int) $iLimit;
        if ( !isset( $this->_aVariantSelections[$iLimit] ) ) {
            $aVariantSelections = false;
            if ( $this->oxarticles__oxvarcount->value ) {
                $oVariants = $this->getVariants( false );
                $aVariantSelections = oxNew( "oxVariantHandler" )->buildVariantSelections( $this->oxarticles__oxvarname->getRawValue(), $oVariants, $aFilterIds, $sActVariantId, $iLimit );

                if ( !empty($oVariants) && empty( $aVariantSelections['rawselections'] ) ) {
                    $aVariantSelections = false;
                }
            }
            $this->_aVariantSelections[$iLimit] = $aVariantSelections;
        }
        return $this->_aVariantSelections[$iLimit];
    }
    */

    public function getVariantHex(){
        return $this->getVariants(true,true);
    }
}
