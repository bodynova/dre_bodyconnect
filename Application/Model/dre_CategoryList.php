<?php

namespace Bender\dre_BodyConnect\Application\Model;

use OxidEsales\EshopCommunity\Application\Model\Category;

class dre_CategoryList extends dre_CategoryList_parent
{

	/**
	 * Aktuelle Welt
	 *
	 * @var int
	 */
	protected $_actWelt = 0;

	protected $_dreActivePath;


	public function setWelt($weltid)
	{
		$this->_actWelt = $weltid;
		return $this->_actWelt;
	}

	public function getWelt($weltid = null)
	{
		if ($this->_actWelt === 0) {
			$this->setWelt($weltid);
		}
		return $this->_actWelt;
	}


	public function getActWelt()
	{
		return $this->_actWelt;
	}

	/**
	 * Fetches raw categories and does postprocessing for adding depth information
	 * // loadList()
	 */
	public function dre_loadWeltList($weltid)
	{
		startProfile('buildCategoryList');

		$this->setLoadFull(false);
		//return $this->dre_getSelectString(false, null, 'oxparentid, oxsort, oxtitle', $weltid);
		$this->selectString($this->dre_getSelectString(false, null, 'oxparentid, oxsort, oxtitle', $weltid)); //$this->selectString($this->_getSelectString(false, null, 'oxparentid, oxsort, oxtitle'));

		// build tree structure
		$this->dre_ppBuildTree();

		// PostProcessing
		// add tree depth info
		$this->dre_ppAddDepthInformation($weltid);
		$this->setWelt($weltid);
		stopProfile('buildCategoryList');
		return $this;
	}

	public function dre_getWeltString($welt = 0)
	{
		if ($welt !== 0) {
			return 'welt' . $welt;
		} else {
			return 0;
		}
	}

	/**
	 * Category list postprocessing routine, responsible for making flat category tree and adding depth information.
	 * Requires reversed category list!
	 */
	public function dre_ppAddDepthInformation($weltid = null)
	{
		$aTree = [];
		$test = [];
		foreach ($this->_aArray as $oCat) {
			// Nur die Kategorien mit der richtigen Welt dem Array zuordnen
			if ($oCat->oxcategories__welt->value !== null && $oCat->oxcategories__welt->value == $weltid) {
				//$test[$oCat->oxcategories__oxid->value] =  $oCat->oxcategories__welt->value;
				$aTree[$oCat->getId()] = $oCat;
				$aSubCats = $oCat->getSubCats();
				if (count($aSubCats) > 0) {
					foreach ($aSubCats as $oSubCat) {
						$aTree = $this->_addDepthInfo($aTree, $oSubCat);
					}
				}
			} else {
				if ($oCat->oxcategories__welt->value == $weltid) {
					//return $oCat->oxcategories__oxid->value;
					$aTree[$oCat->getId()] = $oCat;
					$aSubCats = $oCat->getSubCats();
					if (count($aSubCats) > 0) {
						foreach ($aSubCats as $oSubCat) {
							$aTree = $this->_addDepthInfo($aTree, $oSubCat);
						}
					}
				}
			}


		}

		if ($weltid == null) {
			$this->setWelt(1);
		}

		return $this->_aArray;
		$this->assign($aTree);

	}

	/**
	 * Fetches reversed raw categories and does all necessary postprocessing for
	 * removing invisible or forbidden categories, building oc navigation path,
	 * adding content categories and building tree structure.
	 *
	 * @param string $sActCat Active category (default null)
	 */
	public function dre_buildTree($sActCat)
	{
		startProfile("buildTree");

		$this->_sActCat = $sActCat;
		$this->load();

		// PostProcessing
		if (!$this->isAdmin()) {
			// remove inactive categories
			$this->_ppRemoveInactiveCategories();

			// add active cat as full object
			$this->_ppLoadFullCategory($sActCat);

			// builds navigation path
			$this->_ppAddPathInfo();

			// add content categories
			$this->_ppAddContentCategories();

			// build tree structure
			$this->dre_ppBuildTree();
		}

		stopProfile("buildTree");
	}

	/**
	 * Category list postprocessing routine, responsible building an sorting of hierarchical category tree
	 */
	protected function dre_ppBuildTree($weltid = null)
	{
		$aTree = [];

		$this->setWelt($weltid);

		$active = false;

		if (in_array($this->_sActCat, $this->_aArray)) {
			$active = true;
		}

		foreach ($this->_aArray as $oCat) {
			//$oCat->setExpanded($active);
			$sParentId = $oCat->oxcategories__oxparentid->value;
			if ($weltid != null) {
				$sWelt = $oCat->oxcategories__welt->value;
				if ($sParentId !== 'oxrootid' && $sWelt === $weltid) {
					//return $oCat;
					if (isset($this->_aArray[$sParentId])) {
						$this->_aArray[$sParentId]->setSubCat($oCat, $oCat->getId());
					}
				} else {
					$aTree[$oCat->getId()] = $oCat;
				}
			} else {
				if ($sParentId !== 'oxrootid') {
					if (isset($this->_aArray[$sParentId])) {
						$this->_aArray[$sParentId]->setSubCat($oCat, $oCat->getId());
					}
				} else {
					$aTree[$oCat->getId()] = $oCat;
				}
			}
		}
		$this->assign($aTree);
	}

	public function dre_getActive($id = null)
	{


		//$sql = "SELECT OXID,OXPARENTID FROM oxcategories WHERE OXID = $id";
		//return $this->_aPath;

		if ($id === $this->_sActCat) {
			return true;
		}

		if (array_key_exists($id, $this->_aPath)) {
			return true;
		}

		/*
		$myOcat = oxNew(Category::class);

		$myOcat->load($id);

		$sCurrentCat = $myOcat->oxcategories__parentid->value;
		$sParentId = null;

		while ($sCurrentCat !== 'oxrootid' ) {
			if($myOcat->oxcategories__oxid->value === $this->_sActCat || $myOcat->oxcategories__oxparentid->value === $this->_sActCat){
				return true;
			}

			$oCat = $this[$sCurrentCat];
			$oCat->setExpanded(true);
			$sCurrentCat = $oCat->oxcategories__oxparentid->value;
		}


		foreach ($this->_aArray as $oCat) {
			if($oCat->oxcategories__oxid->value == $id || $oCat->oxcategories__oxparentid->value == $id ){
				return true;
			}
		}
		*/

		return false;

		echo '<pre>';
		print_r($this->_sActCat);
		die();


		/*
		$myOcat = oxNew(Category::class);

		$myOcat->load($this->_sActCat);


		while ($sCurrentCat != 'oxrootid' && isset($this[$sCurrentCat])) {
			$oCat = $this[$sCurrentCat];
			$oCat->setExpanded(true);
			$sCurrentCat = $oCat->oxcategories__oxparentid->value;
		}

		/*
		foreach ($this->_aArray as $oCat) {
			if($oCat->oxcategories__oxid->value === $this->_sActCat){
				return true;
			}
		}

		// $aPath = [];
		// $sCurrentCat = $this->_sActCat;

		/**
		 *
		 * while ($sCurrentCat != 'oxrootid' && isset($this[$sCurrentCat])) {
		 * $oCat = $this[$sCurrentCat];
		 * $oCat->setExpanded(true);
		 * $aPath[$sCurrentCat] = $oCat;
		 * $sCurrentCat = $oCat->oxcategories__oxparentid->value;
		 * }
		 *
		 * $this->_aPath = array_reverse($aPath);
		 *
		 */

		return false;

	}

	/**
	 * constructs the sql string to get the category list
	 *
	 * @param bool $blReverse list loading order, true for tree, false for simple list (optional, default false)
	 * @param array $aColumns required column names (optional)
	 * @param string $sOrder order by string (optional)
	 *
	 * @return string
	 */
	protected function dre_getSelectString($blReverse = false, $aColumns = null, $sOrder = null, $sWeltid = null)
	{
		$sViewName = $this->getBaseObject()->getViewName();
		print_r($aColumns);
		if (isset($aColumns) && count($aColumns) > 0) {
			$aColumns['welt'] = $sWeltid;
		}
		$sFieldList = $this->dre_getSqlSelectFieldsForTree($sViewName, $aColumns);
		//excluding long desc
		if (!$this->isAdmin() && !$this->_blHideEmpty && !$this->getLoadFull()) {
			$oCat = oxNew(\OxidEsales\Eshop\Application\Model\Category::class);
			if (!($this->_sActCat && $oCat->load($this->_sActCat) && $oCat->oxcategories__oxrootid->value)) {
				$oCat = null;
				$this->_sActCat = null;
			}

			$sUnion = $this->dre_getDepthSqlUnion($oCat, $aColumns);
			$sWhere = $this->dre_getDepthSqlSnippet($oCat);
		} else {
			$sUnion = '';
			$sWhere = '1';

			if ($sWeltid !== null) {
				$sWhere .= ' AND welt = ' . $sWeltid;
			}
			$sWhere .= ' AND oxhidden = 0';
		}

		if (!$sOrder) {
			$sOrdDir = $blReverse ? 'desc' : 'asc';
			$sOrder = "oxrootid $sOrdDir, oxleft $sOrdDir";
		}

		$this->setWelt($sWeltid);

		return "select $sFieldList from $sViewName where $sWhere $sUnion order by $sOrder";
	}

	/**
	 * returns sql snippet for union of select category's and its upper level
	 * siblings of the same root (siblings of the category, and parents and
	 * grandparents etc)
	 *
	 * @param \OxidEsales\Eshop\Application\Model\Category $oCat current category object
	 * @param array $aColumns required column names (optional)
	 *
	 * @return string
	 */
	protected function dre_getDepthSqlUnion($oCat, $aColumns = null)
	{
		if (!$oCat) {
			return '';
		}

		$sViewName = $this->getBaseObject()->getViewName();

		return "UNION SELECT " . $this->_getSqlSelectFieldsForTree('maincats', $aColumns)
			. " FROM oxcategories AS subcats"
			. " LEFT JOIN $sViewName AS maincats on maincats.oxparentid = subcats.oxparentid"
			. " WHERE subcats.oxrootid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($oCat->oxcategories__oxrootid->value)
			. " AND subcats.oxleft <= " . (int)$oCat->oxcategories__oxleft->value
			. " AND subcats.oxright >= " . (int)$oCat->oxcategories__oxright->value;
	}

	/**
	 * constructs the sql snippet responsible for depth optimizations,
	 * loads only selected category's siblings
	 *
	 * @param \OxidEsales\Eshop\Application\Model\Category $oCat selected category
	 *
	 * @return string
	 */
	protected function dre_getDepthSqlSnippet($oCat)
	{
		$sViewName = $this->getBaseObject()->getViewName();
		$sDepthSnippet = ' ( 0';

		// load complete tree of active category, if it exists
		if ($oCat) {
			// select children here, siblings will be selected from union
			$sDepthSnippet .= " or ($sViewName.oxparentid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($oCat->oxcategories__oxid->value) . ")";
		}

		// load 1'st category level (roots)
		if ($this->getLoadLevel() >= 1) {
			$sDepthSnippet .= " or $sViewName.oxparentid = 'oxrootid'";
		}

		// load 2'nd category level ()
		if ($this->getLoadLevel() >= 2) {
			$sDepthSnippet .= " or $sViewName.oxrootid = $sViewName.oxparentid or $sViewName.oxid = $sViewName.oxrootid";
		}

		$sDepthSnippet .= ' ) ';

		return $sDepthSnippet;
	}

	/**
	 * return fields to select while loading category tree
	 *
	 * @param string $sTable table name
	 * @param array $aColumns required column names (optional)
	 *
	 * @return string return
	 */
	protected function dre_getSqlSelectFieldsForTree($sTable, $aColumns = null)
	{
		if ($aColumns && count($aColumns)) {
			foreach ($aColumns as $key => $val) {
				$aColumns[$key] .= ' as ' . $val;
			}

			return "$sTable." . implode(", $sTable.", $aColumns);
		}

		$sFieldList = "$sTable.oxid as oxid, $sTable.oxactive as oxactive,"
			. " $sTable.oxhidden as oxhidden, $sTable.oxparentid as oxparentid,"
			. " $sTable.oxdefsort as oxdefsort, $sTable.oxdefsortmode as oxdefsortmode,"
			. " $sTable.oxleft as oxleft, $sTable.oxright as oxright,"
			. " $sTable.oxrootid as oxrootid, $sTable.oxsort as oxsort,"
			. " $sTable.oxtitle as oxtitle, $sTable.oxdesc as oxdesc,"
			. " $sTable.oxpricefrom as oxpricefrom, $sTable.oxpriceto as oxpriceto,"
			. " $sTable.oxicon as oxicon, $sTable.oxextlink as oxextlink,"
			. " $sTable.welt as welt,"
			. " $sTable.oxthumb as oxthumb, $sTable.oxpromoicon as oxpromoicon";

		$sFieldList .= $this->dre_getActivityFieldsSql($sTable);

		return $sFieldList;
	}

	/**
	 * Get activity related fields
	 *
	 * @param string $tableName
	 *
	 * @return string SQL snippet
	 */
	protected function dre_getActivityFieldsSql($tableName)
	{
		return ",not $tableName.oxactive as oxppremove";
	}


	/**
	 * Recursive function to add depth information
	 *
	 * @param array $aTree new category tree
	 * @param object $oCat category object
	 * @param string $sDepth string to show category depth
	 *
	 * @return array $aTree
	 */
	protected function _addDepthInfo($aTree, $oCat, $sDepth = "")
	{
		// $sDepth .= "-";
		$oCat->oxcategories__oxtitle->setValue($sDepth . ' ' . $oCat->oxcategories__oxtitle->value);
		$aTree[$oCat->getId()] = $oCat;
		$aSubCats = $oCat->getSubCats();
		if (count($aSubCats) > 0) {
			foreach ($aSubCats as $oSubCat) {
				$aTree = $this->_addDepthInfo($aTree, $oSubCat, $sDepth);
			}
		}

		return $aTree;
	}
}