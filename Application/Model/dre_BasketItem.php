<?php

namespace Bender\dre_BodyConnect\Application\Model;


class dre_BasketItem extends dre_BasketItem_parent
{
    public $arrUsedDiscounts = array();

    public function getRegularUnitPrice()
    {
        //
        $ret = $this->_oRegularUnitPrice;

        if($ret->dPriceBeforeStaffel) {
            $ret->setPrice($ret->dPriceBeforeStaffel);
        }
        return $ret;
    }



    public function clearUsedDiscount()
    {
        $this->arrUsedDiscounts = array();
    }

    public function addUsedDiscount($strName, $dValue)
    {
        // initialisieren wenn nicht gesetzt
        if(!isset($this->arrUsedDiscounts[$strName]))
            $this->arrUsedDiscounts[$strName] = 0;
        //
        $this->arrUsedDiscounts[$strName]+= $dValue *  $this->getAmount();

        $article = $this->getArticle( true );

        $article->usedDiscounts = $this->arrUsedDiscounts;
        // ende
        /*

        echo '<pre>';
        print_r($this->arrUsedDiscounts);
        print_r($strName);
        print_r($dValue);
        print_r($this->getAmount());
        echo '</pre>';
        */
    }


}