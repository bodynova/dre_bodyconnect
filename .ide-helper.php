<?php
namespace Bender\dre_BodyConnect\Application\Controller;
class dre_recommarticlelistcontroller extends \OxidEsales\Eshop\Application\Controller\ArticleListController{}
class bnversand extends \OxidEsales\Eshop\Application\Controller\FrontendController{}
class dredetails extends \OxidEsales\Eshop\Application\Controller\ArticleDetailsController{}
class dre_NewsletterController extends \OxidEsales\Eshop\Application\Controller\NewsletterController{}
class dre_ArticleListController extends \OxidEsales\Eshop\Application\Controller\ArticleListController{}

namespace Bender\dre_BodyConnect\Application\Controller\Admin;
class dre_category_main extends \OxidEsales\Eshop\Application\Controller\Admin\CategoryMain{}
class dre_category_main_ajax extends \OxidEsales\Eshop\Application\Controller\Admin\CategoryMainAjax{}
class dre_orderoverview extends \OxidEsales\Eshop\Application\Controller\Admin\OrderOverview{}
class dre_ordermain extends \OxidEsales\Eshop\Application\Controller\Admin\OrderMain{}

namespace Bender\dre_BodyConnect\Application\Model;
class dre_ArticleList extends \OxidEsales\Eshop\Application\Model\ArticleList{}
class dre_Article extends \OxidEsales\Eshop\Application\Model\Article{}
class dre_Basket extends \OxidEsales\Eshop\Application\Model\Basket{}
class dre_BasketItem extends \OxidEsales\Eshop\Application\Model\BasketItem{}
class dre_CategoryList extends \OxidEsales\Eshop\Application\Model\CategoryList{}

namespace Bender\dre_BodyConnect\Application\Component\Widget;
class dre_CategoryTree extends \OxidEsales\Eshop\Application\Component\Widget\CategoryTree{}

namespace Bender\dre_BodyConnect\Core;
class dre_Price extends \OxidEsales\Eshop\Core\Price{}