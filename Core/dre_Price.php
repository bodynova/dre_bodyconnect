<?php

namespace Bender\dre_BodyConnect\Core;

class dre_Price extends dre_Price_parent {
    /**
     * Artikelpreis bevor irgend ein Rabatt angewendet wird.
     * @var null
     */
    public $dPriceBeforeStaffel = null;

    /**
     * Summe der angewendeten Rabatte des Artikels
     * @var null
     */
    public $usedDiscounts = null;


    /**
     *
     */
    public function calculateDiscount()
    {
        $dPrice = $this->getPrice();
        $aDiscounts = $this->getDiscounts();

        #var_dump($aDiscounts);
        if ($aDiscounts) {
            //
            $dPrice = $this->dPriceBeforeStaffel;
            //
            #echo 'Normalpreis: ' . $dPrice.'<br>';

            foreach ($aDiscounts as $aDiscount) {
                if ($aDiscount['type'] === 'abs') {
                    $dPrice -=  $aDiscount['value'];
                } else {
                    $dPrice = $dPrice * (100 - $aDiscount['value']) / 100;
                }
            }
            //
            if($dPrice > $this->getPrice())
                $dPrice = $this->getPrice();
            //
            if ($dPrice < 0) {
                $this->setPrice(0);
            } else {
                $this->setPrice($dPrice);
            }
            // Variable used Discounts setzen
            $this->usedDiscounts = $this->dPriceBeforeStaffel - $dPrice;
            #echo 'Endgültiger Preis: '.$dPrice.'<br>';
            $this->_flushDiscounts();
        }
    }
}
