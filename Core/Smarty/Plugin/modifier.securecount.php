<?php
function smarty_modifier_securecount($array)
{
    if (is_countable($array) && count($array) > 0) {
        return count($array);
    }
    return false;
}
